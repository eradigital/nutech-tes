const path = require('path');
module.exports = function override(config) {
  config.resolve = {
    ...config.resolve,
    alias: {
      ...config.alias,
      '@nutech-core': path.resolve(__dirname, 'src/core'),
      '@nutech-presentation': path.resolve(__dirname, 'src/presentation'),
      '@nutech-infrastructure': path.resolve(__dirname, 'src/infrastructure'),
      '@nutech-domain': path.resolve(__dirname, 'src/domain'),
      '@nutech-application': path.resolve(__dirname, 'src/application')
    },
  };
return config;
};