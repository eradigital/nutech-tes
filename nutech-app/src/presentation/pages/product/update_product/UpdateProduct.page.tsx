import React from "react";
import TemplatePage from "@nutech-presentation/templates";
import SectionFormAddUser from "./sections/SectionFormUpdateProduct";
import ComponentTitle from "@nutech-presentation/components/shared/ComponentTitle";

export default React.memo(function UpdateProductPage() {
  return (
    <main className="reset">
      <TemplatePage>
        <div className="px-5 pt-4">
          <ComponentTitle
            title="Update Produk"
            desc="Silahkan isi form untuk memperbaharui data produk"
          />
          <SectionFormAddUser />
        </div>
      </TemplatePage>
    </main>
  );
});
