import React from "react";
import TemplatePage from "@nutech-presentation/templates";
import SectionFormAddUser from "./sections/SectionFormAddProduct";
import ComponentTitle from "@nutech-presentation/components/shared/ComponentTitle";

export default React.memo(function CreateProductPage() {
  return (
    <main className="reset">
      <TemplatePage>
        <div className="px-5 pt-4">
          <ComponentTitle
            title="Tambahkan Produk"
            desc="Silahkan isi form untuk menambahkan produk baru"
          />
          <SectionFormAddUser />
        </div>
      </TemplatePage>
    </main>
  );
});
