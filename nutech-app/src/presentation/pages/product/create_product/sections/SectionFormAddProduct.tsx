import React, { useState, useEffect } from "react";
import ComponentInput from "@nutech-presentation/components/form_handler/ComponentInput";
import ComponentSubmitButton from "@nutech-presentation/pages/signin/components/ComponentSubmitButton";
import onsubmit_form from "@nutech-core/utils/onsubmit_form";
import { useSelector, useDispatch } from "react-redux";
import "react-datepicker/dist/react-datepicker.css";
import { useHistory } from "react-router-dom";
import ComponentDropBoxImage from "@nutech-presentation/components/form_handler/ComponentDropBoxImage";
import CenterRow from "@nutech-presentation/components/shared/CenterRow";
import { createProductMiddleware } from "@nutech-application/redux/product/middlewares/create_produk.middleware";
import ComponentCurrency from "@nutech-presentation/components/form_handler/ComponentCurrency";
import { clearFormHandling } from "@nutech-application/redux/form_handling/actions/clear.action";

export default React.memo(function SectionForm() {
  const [image, setImage] = useState<any>(null);
  const [productName, setProductName] = useState("");
  const [productStock, setProductStock] = useState(0);
  const [purchasePrice, setPurchasePrice] = useState(0);
  const [sellingPrice, setSellingPrice] = useState(0);
  const dispatch = useDispatch();
  const stateRedux: any = useSelector((state) => state);
  let history = useHistory();

  useEffect(() => {
    return () => {
      dispatch(clearFormHandling());
    }
  }, [dispatch])

  const onClick = (e: any) => {
    e.preventDefault();
    if (onsubmit_form(dispatch, stateRedux)) {
      const data = {
        file_image_id: image.id,
        name_product: productName,
        purchase_price: purchasePrice,
        selling_price: sellingPrice,
        stock: productStock
      };

      dispatch(createProductMiddleware(data, history));
    }
  };

  const handleChangeName = React.useCallback((text: any, isValid: any) => {
    setProductName(text);
  }, []);

  const handleChangeStock = React.useCallback((text: any, isValid: any) => {
    setProductStock(text);
  }, []);

  const handleChangePurchase = React.useCallback((text: any, isValid: any) => {
    setPurchasePrice(text);
  }, []);

  const handleChangeSelling = React.useCallback((text: any, isValid: any) => {
    setSellingPrice(text);
  }, []);

  return (
    <div className="mt-3">
      <ComponentDropBoxImage
        onChange={(value: any) => {
          setImage(value);
        }}
      />
      <div className="row col-md-12 reset d-flex justify-content-between">
        <div style={{ width: "47%" }} className="mt-5 col-md-6 reset pr-0">
          <ComponentInput
            key={'1'}
            form="add_product"
            message="Mohon masukan nama produk"
            label="Masukan Nama Produk"
            name="product_name"
            required
            type="text"
            onChangeText={handleChangeName}
          />
        </div>
        <div style={{ width: "47%" }} className="mt-5 col-md-6 reset pr-0">
          <ComponentInput
            key={'2'}
            form="add_product"
            message="Mohon masukan jumlah stok"
            label="Masukan Stok Produk"
            name="product_stock"
            required
            type="number"
            onChangeText={handleChangeStock}
          />
        </div>
      </div>
      <div className="row col-md-12 reset d-flex justify-content-between">
        <div style={{ width: "47%" }} className="mt-5 col-md-6 reset pr-0">
          <ComponentCurrency
            key={'3'}
            form="add_product"
            message="Mohon masukan harga beli produk"
            label="Masukan Harga Beli Produk"
            name="purchase_price"
            required
            onChangeText={handleChangePurchase}
          />
        </div>
        <div style={{ width: "47%" }} className="mt-5 col-md-6 reset pr-0">
          <ComponentCurrency
            key={'4'}
            form="add_product"
            message="Mohon masukan harga jual produk"
            label="Masukan Harga Jual Produk"
            name="selling_price"
            required
            onChangeText={handleChangeSelling}
          />
        </div>
      </div>
      <br />
      <br />
      <div className="col-md-12">
        <CenterRow>
          <div className="mt-5 col-md-6">
            <ComponentSubmitButton onClick={onClick} isLoading={false} />
          </div>
        </CenterRow>
      </div>
    </div>
  );
});
