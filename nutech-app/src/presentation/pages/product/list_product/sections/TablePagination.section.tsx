import React from "react";
import TablePagination from "@material-ui/core/TablePagination";

export default React.memo(function TablePaginationSection({
  productsData,
  rowsPerPage,
  page,
  handleChangePage,
  handleChangeRowsPerPage
}: {
  productsData: any;
  rowsPerPage: number;
  page: number;
  handleChangePage: (event: any, page: number) => void;
  handleChangeRowsPerPage: any;
}) {
  return (
    <TablePagination
      rowsPerPageOptions={[]}
      labelRowsPerPage={<div />}
      component="div"
      count={productsData?.total ?? 0}
      rowsPerPage={rowsPerPage}
      page={page}
      onPageChange={handleChangePage}
      onRowsPerPageChange={handleChangeRowsPerPage}
    />
  );
});
