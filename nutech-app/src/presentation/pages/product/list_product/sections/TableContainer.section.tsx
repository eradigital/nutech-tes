import React from "react";
import Table from "@material-ui/core/Table";
import TableContainer from "@material-ui/core/TableContainer";
import TableHeaderComponent from "../components/TableHeader.component";
import TableBodyComponent from "../components/TableBody.component";

export default React.memo(function TabelContainerSection({
  classes,
  products,
  page,
  rowsPerPage
}: {
  classes: any;
  products: [];
  page: number;
  rowsPerPage: number;
}) {
  return (
    <TableContainer className={classes.container}>
      <Table stickyHeader aria-label="sticky table">
        <TableHeaderComponent />
        <TableBodyComponent
          products={products}
          page={page}
          rowsPerPage={rowsPerPage}
        />
      </Table>
    </TableContainer>
  );
});
