import { deleteProductRepository } from "@nutech-domain/repository/product/delete_product.repository";

export const deleteProduct = async (products: [], id: number, callBack: (value: any) => void) => {
  let unLoad = products.filter((el: any) => el.id !== id);
  const result = await deleteProductRepository(id.toString());
  if(result?.success){
      callBack(unLoad);
  }
};
