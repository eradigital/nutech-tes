import React from "react";
import Paper from "@material-ui/core/Paper";
import TemplatePage from "../../../templates/index";
import ComponentTitle from "@nutech-presentation/components/shared/ComponentTitle";
import { getListProductMiddleware } from "../../../../application/redux/product/middlewares/get_list_product.middleware";
import { useHistory } from "react-router-dom";
import { useSelector, useDispatch } from "react-redux";
import { RootState } from "../../../../application/redux/root.reducer";
import TablePaginationSection from "./sections/TablePagination.section";
import TabelContainerSection from "./sections/TableContainer.section";
import { useStyles } from "./styles/list_product.style";
import ComponentInput from "@nutech-presentation/components/form_handler/ComponentInput";
import ComponentCircleButton from "@nutech-presentation/components/buttons/ComponentCircleButton";
import { clearFormHandling } from "@nutech-application/redux/form_handling/actions/clear.action";

export default React.memo(function ListProductPage() {
  const classes = useStyles();
  const [page, setPage] = React.useState(0);
  const [keyword, setKeyword] = React.useState("");
  const [rowsPerPage, setRowsPerPage] = React.useState(5);
  const [listProduct, setListProduct] = React.useState<any>([]);
  const dispatch = useDispatch();
  const history = useHistory();
  const stateRedux: any = useSelector((state: RootState) => state);
  const productsData = stateRedux.product.posts;
  const products = productsData ? productsData.data : [];

  const memoizeHandleChangePage = React.useCallback(
    (event: unknown, newPage: number) => {
      dispatch(getListProductMiddleware(history, newPage + 1, keyword));
      setPage(newPage);
    },
    [dispatch, history, keyword]
  );

  const memoizeHandleChangeRowsPerPage = React.useCallback(
    (event: React.ChangeEvent<HTMLInputElement>) => {
      setRowsPerPage(+event.target.value);
      setPage(0);
    },
    []
  );

  React.useEffect(() => {
    const countListProduct = listProduct.length / 5;

    if (countListProduct - page === 0) {
      setListProduct([...products, ...listProduct].reverse());
    }
    return () => {
      setListProduct([]);
    };
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [productsData]);

  React.useEffect(() => {
    dispatch(getListProductMiddleware(history, page, keyword));
    return () => {
      dispatch(clearFormHandling());
    };
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const memoizedProducts = React.useMemo(() => productsData, [productsData]);
  const memoizedRowsPerPage = React.useMemo(() => rowsPerPage, [rowsPerPage]);
  const memoizedPage = React.useMemo(() => page, [page]);
  const memoizedListProduct = React.useMemo(() => listProduct, [listProduct]);
  const memoizedClasses = React.useMemo(() => classes, [classes]);

  const handleSetKeyword = React.useCallback((text: any, isValid: any) => {
    setKeyword(text);
    dispatch(getListProductMiddleware(history, page, text));
  // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return (
    <TemplatePage>
      <div className="m-5">
        <ComponentTitle
          title="Daftar Produk"
          desc="Daftar semua produk yang telah terdaftar"
        />
        <div className="mb-5 col-md-4 reset d-flex pr-0">
          <div className="col-md-10 reset">
            <ComponentInput
              key={"1"}
              form="search"
              message=""
              label="Masukan Pencarian Dengan Nama Produk"
              name="keyword"
              required
              type="text"
              onChangeText={handleSetKeyword}
            />
          </div>
          <div className="col-md-2 reset align-items-end d-flex">
            <ComponentCircleButton
              onClick={() => {}}
              icon="fa fa-search"
              className="me-3"
            />
          </div>
        </div>
        <Paper className={memoizedClasses.root}>
          <TabelContainerSection
            classes={memoizedClasses}
            products={memoizedListProduct}
            page={memoizedPage}
            rowsPerPage={memoizedRowsPerPage}
          />
          <TablePaginationSection
            productsData={memoizedProducts}
            rowsPerPage={memoizedRowsPerPage}
            page={memoizedPage}
            handleChangePage={memoizeHandleChangePage}
            handleChangeRowsPerPage={memoizeHandleChangeRowsPerPage}
          />
        </Paper>
      </div>
    </TemplatePage>
  );
});
