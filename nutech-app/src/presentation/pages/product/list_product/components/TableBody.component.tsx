import React, { useEffect } from "react";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableRow from "@material-ui/core/TableRow";
import { columns } from "../constants/header_column.constant";
import { LazyLoadImage } from "react-lazy-load-image-component";
import ComponentPillButton from "@nutech-presentation/components/buttons/ComponentPillButton";
import { ROUTES } from "@nutech-core/constants/routes/list_route.constant";
import { alert_confirm } from "@nutech-core/utils/alert_notification.util";
import { deleteProduct } from "../functions/delete_product.function";
import { useHistory } from "react-router-dom";

export default React.memo(function TableBodyComponent({
  products,
  page,
  rowsPerPage
}: {
  products: [];
  page: number;
  rowsPerPage: number;
}) {
  const [listProduct, setListProduct] = React.useState(products);
  const history = useHistory();

  useEffect(() => {
    setListProduct(products);
    return () => {};
  }, [products]);

  return (
    <TableBody>
      {listProduct
        .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
        .map((row: any) => {
          return (
            <TableRow hover role="checkbox" tabIndex={-1} key={row.id}>
              {columns.map((column: any) => {
                const value = row[column.id];
                if (column.id === "file_image") {
                  return (
                    <TableCell key={column.id} align={column.align}>
                      <LazyLoadImage
                        alt={"alt"}
                        height={60}
                        src={value.file_url}
                        width={60}
                      />
                    </TableCell>
                  );
                } else if (column.id === "id") {
                  return (
                    <TableCell key={column.id} align={column.align}>
                      <ComponentPillButton
                        label="Edit"
                        onClick={() => {
                          history.push(ROUTES.UPDATE_PRODUCT, row);
                        }}
                        className="bg-success me-3"
                        isLoading={false}
                      />
                      <ComponentPillButton
                        label="Delete"
                        onClick={() => {
                          alert_confirm(
                            "Apakah anda yakin ingin menghapus data produk ?",
                            () => {
                              deleteProduct(listProduct, value, (value) => {
                                setListProduct(value);
                              });
                            }
                          );
                        }}
                        className="bg-danger"
                        isLoading={false}
                      />
                    </TableCell>
                  );
                } else {
                  return (
                    <TableCell key={column.id} align={column.align}>
                      {column.format && typeof value === "number"
                        ? column.format(value)
                        : value}
                    </TableCell>
                  );
                }
              })}
            </TableRow>
          );
        })}
    </TableBody>
  );
});
