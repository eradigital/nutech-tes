export interface Column {
  id: "file_image" | "name_product" | "purchase_price" | "selling_price" | "stock" | "id";
  label: string;
  minWidth?: number;
  align?: "right";
  format?: (value: number) => string;
}
