import { Column } from "../models/header_column.model";

export const columns: Column[] = [
  { id: "file_image", label: "Image", minWidth: 170 },
  { id: "name_product", label: "Nama Produk", minWidth: 100 },
  {
    id: "purchase_price",
    label: "Harga Beli",
    minWidth: 170,
    align: "right",
    format: (value: number) => value.toLocaleString("en-US")
  },
  {
    id: "selling_price",
    label: "Harga Juat",
    minWidth: 170,
    align: "right",
    format: (value: number) => value.toLocaleString("en-US")
  },
  {
    id: "stock",
    label: "Stok",
    minWidth: 170,
    align: "right",
    format: (value: number) => value.toLocaleString("en-US")
  },
  {
    id: "id",
    label: "Action",
    minWidth: 170,
    align: "right"
  }
];
