import React from "react";
import SectionLeftContent from "./sections/SectionLeftContent";
import SectionRightContent from "./sections/SectionRightContent";

export default React.memo(function Index() {
  return (
    <section id="form-login" className="">
      <div className="">
        <div className="row">
          <SectionLeftContent />
          <SectionRightContent />
        </div>
      </div>
    </section>
  );
});
