import React, { useState, useEffect } from "react";
import ComponentSubmitButton from "../components/ComponentSubmitButton";
import ComponentSignInWithGoogle from "../components/ComponentSignInWithGoogle";
import ComponentRememberMeForgotPassword from "../components/ComponentRememberMeForgotPassword";
import ComponentTitle from "../components/ComponentTitle";
import ComponentInput from "@nutech-presentation/components/form_handler/ComponentInput";
import { useSelector, useDispatch } from "react-redux";
import { userAuthentication } from "@nutech-application/redux/auth/middlewares/userAuthentication.middleware";
import { useHistory } from "react-router-dom";
import onsubmit_form from "@nutech-core/utils/onsubmit_form";
import CenterRow from "@nutech-presentation/components/shared/CenterRow";
import { ROUTES } from "@nutech-core/constants/routes/list_route.constant";

export default React.memo(function SectionRightContent() {
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const dispatch = useDispatch();
  const stateRedux: any = useSelector((state) => state);
  const stateAuth = stateRedux.auth;
  const history = useHistory();

  useEffect(() => {
    if (stateAuth.posts) {
      history.push(ROUTES.LIST_PRODUCT);
    }
    return () => {};
  }, [history, stateAuth]);

  const onClick = (e: any) => {
    e.preventDefault();
    if (onsubmit_form(dispatch, stateRedux)) {
      const data = {
        email: email,
        password: password
      };
      dispatch(userAuthentication(data, history));
    }
  };

  return (
    <div className="col-md-5 d-flex justify-content-md-center">
      <form className="col-md-9 p-0 pb-5 pt-5 p-sm-5 bg-white shadow my-5">
        <div className="px-5">
          <CenterRow>
            <ComponentTitle />
          </CenterRow>
          <br />
          <ComponentInput
            form="login"
            message="Mohon masukan alamat email"
            label="Email"
            name="email"
            required
            type="text"
            onChangeText={(text: any, isValid: any) => {
              setEmail(text);
            }}
          />
          <ComponentInput
            form="login"
            message="Mohon masukan password"
            label="Password"
            name="password"
            required
            type="password"
            onChangeText={(text: any, isValid: any) => {
              setPassword(text);
            }}
          />
        </div>
        <ComponentRememberMeForgotPassword />
        <CenterRow>
          <ComponentSubmitButton onClick={onClick} isLoading={false} />
        </CenterRow>
        <ComponentSignInWithGoogle />
      </form>
    </div>
  );
});
