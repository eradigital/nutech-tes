import React from "react";
import CenterContent from "@nutech-presentation/components/shared/CenterContent";

export default React.memo(function SectionLeftContent() {
  return (
    <div className="col-md-7 pt-5 d-flex flex-column pl-5 left-content">
      <CenterContent>
        <img
          src="/images/backgrounds/login.png"
          alt="login key developer aplication web mobile"
          width="60%"
        />
        <h3 className="font-weight-bold my-4 text-white">Admin NuTech</h3>
        <p className="mx-5 text-black text-white">
          Silahkan login untuk masuk ke halaman admin
        </p>
      </CenterContent>
    </div>
  );
});
