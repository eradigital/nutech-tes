import React from "react";

export default React.memo(function ComponentSignInWithGoogle() {
  return (
    <div className="text-center">
      <div className="d-flex align-items-center justify-content-center mt-5">
        <hr className="flex-1 w-100" />
        <span className="mx-3">|</span>
        <hr className="flex-1 w-100" />
      </div>
      <p className="mt-3 mb-5 size_30">Silahkan Login Untuk Masuk</p>
    </div>
  );
});
