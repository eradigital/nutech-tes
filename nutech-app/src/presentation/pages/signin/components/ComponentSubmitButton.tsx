import React from "react";
import ComponentPillButton from "@nutech-presentation/components/buttons/ComponentPillButton";

export default React.memo(function ComponentSubmitButton(props: any) {
  const { onClick, isLoading } = props;
  return (
    <ComponentPillButton
      className="b_secondary w-75 py-2"
      label="Submit"
      onClick={onClick}
      isLoading={isLoading}
    />
  );
});
