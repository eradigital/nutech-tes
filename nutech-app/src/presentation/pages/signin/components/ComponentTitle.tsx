import React from "react";

export default React.memo(function ComponentTitle() {
  return (
    <img
      src="/images/svg/user.svg"
      alt="user developer aplication web mobile"
      width="50%"
      className="my-3"
    />
  );
});
