import React from "react";

export default React.memo(function ComponentRememberMeForgotPassword() {
  return (
    <div className="d-flex align-items-center justify-content-between mx-5 px-4 my-4">
      <div className="d-flex align-items-center mb-2 pt-2">
        <input type="checkbox" />
        &nbsp; <span className="size_40">Remember Me!</span>
      </div>
      <a href="/">
        <span className="size_30">Forgot Password!</span>
      </a>
    </div>
  );
});
