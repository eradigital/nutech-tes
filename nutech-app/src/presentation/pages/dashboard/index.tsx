import TemplatePage from "@nutech-presentation/templates";
import ComponentTitle from "@nutech-presentation/components/shared/ComponentTitle";
import React from "react";

export default React.memo(function Index() {
  return (
    <main className="reset">
      <TemplatePage>
        <div className="p-5">
          <ComponentTitle
            title="Dashboard"
            desc="Monitoring data semua karyawan dengan mudah"
          />
        </div>
      </TemplatePage>
    </main>
  );
});
