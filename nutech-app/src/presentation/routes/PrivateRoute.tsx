import React, { Fragment } from "react";
import { Redirect, Route } from "react-router-dom";
import { ROUTES } from "@nutech-core/constants/routes/list_route.constant";

export default React.memo(function PrivateRoute({
  component,
  authed,
  path,
  ...rest
}: any) {
  return (
    <Fragment>
      {authed && <Route {...rest} path={path} component={component} />}
      {!authed && <Redirect to={{ pathname: ROUTES.SIGNIN }} />}
    </Fragment>
  );
});
