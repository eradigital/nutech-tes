import React, { useEffect } from "react";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import PrivateRoute from "./PrivateRoute";

import SignInPage from "@nutech-presentation/pages/signin/SignIn.page";
import ListProductPage from "@nutech-presentation/pages/product/list_product/ListProduct.page";
import CreateProductPage from "@nutech-presentation/pages/product/create_product/CreateProduct.page";
import { ROUTES } from "@nutech-core/constants/routes/list_route.constant";
import { STORAGE } from "@nutech-core/constants/storage/local.storage";
import { restoreToken } from "@nutech-application/redux/auth/middlewares/restoreToken.middleware";
import { useSelector, useDispatch } from "react-redux";
import { reactLocalStorage } from "reactjs-localstorage";
import UpdateProductPage from "@nutech-presentation/pages/product/update_product/UpdateProduct.page";

export default React.memo(function App() {
  const stateRedux: any = useSelector((state) => state);
  const dispatch = useDispatch();
  const auth: any = reactLocalStorage.getObject(STORAGE.AUTH);
  const stateAuth = auth.token ? auth : stateRedux.auth.posts;

  useEffect(() => {
    if (auth.token) {
      dispatch(restoreToken(auth));
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return (
    <Router>
      <Switch>
        <Route path={ROUTES.SIGNIN} exact component={SignInPage} />
        <PrivateRoute
          authed={stateAuth}
          path={ROUTES.LIST_PRODUCT}
          exact
          component={ListProductPage}
        />
        <PrivateRoute
          path={ROUTES.CREATE_PRODUCT}
          authed={stateAuth}
          exact
          component={CreateProductPage}
        />
        <PrivateRoute
          path={ROUTES.UPDATE_PRODUCT}
          authed={stateAuth}
          exact
          component={UpdateProductPage}
        />
        <Route
          path="*"
          component={() => (
            <div
              style={{ height: 100 }}
              className="justify-content-center align-items-center d-flex bg-success"
            >
              <h1 className="text-white">"404 NOT FOUND"</h1>
            </div>
          )}
        />
      </Switch>
    </Router>
  );
});
