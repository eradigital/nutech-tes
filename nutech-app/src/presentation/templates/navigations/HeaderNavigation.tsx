import React from "react";
import { Link } from "react-router-dom";
import ComponentPillButton from "@nutech-presentation/components/buttons/ComponentPillButton";
import { userLogout } from "@nutech-application/redux/auth/middlewares/userLogout.middleware";
import { useSelector, useDispatch } from "react-redux";
import ComponentCircleButton from "@nutech-presentation/components/buttons/ComponentCircleButton";
import { useHistory } from "react-router-dom";
import { ROUTES } from "@nutech-core/constants/routes/list_route.constant";

export default React.memo(function HeaderNavigation(props: any) {
  const { toogleDrawer, action, translate } = props;
  const stateRedux: any = useSelector((state) => state);
  const stateAuth = stateRedux.auth.posts;
  const dispatch = useDispatch();
  const history = useHistory();

  return (
    <section className="bg-white sticky-top w-100">
      <nav id="main_menu">
        <div className="menu_container py-3 px-5">
          <div className="px-5 d-flex justify-content-between">
            <div className="right_menu">
              {stateAuth && action !== "create" && (
                <ComponentCircleButton
                  onClick={toogleDrawer}
                  icon={
                    translate !== "translateLeft"
                      ? "fa fa-arrow-left"
                      : "fa fa-arrow-right"
                  }
                  className="me-3"
                />
              )}
              <ComponentCircleButton
                onClick={() => {
                  history.push(ROUTES.LIST_PRODUCT);
                }}
                icon="fa fa-home"
                className="me-3"
              />
            </div>
            <div className="right_menu">
              {stateAuth && (
                <>
                  <span className="notify_wrapper me-4">
                    <i
                      className="fa fa-bell size_70 c_success"
                      aria-hidden="true"
                    ></i>
                    <span className="counter">1</span>
                  </span>
                  <span className="notify_wrapper me-4">
                    <i className="fas fa-comment size_70 c_success"></i>
                    <span className="counter">1</span>
                  </span>
                  <ComponentPillButton
                    className="b_primary width_10 py-1"
                    label="Logout"
                    onClick={() => {
                      dispatch(userLogout(history));
                    }}
                    isLoading={false}
                  />
                </>
              )}
              {!stateAuth && (
                <Link to="/signin">
                  <ComponentPillButton
                    className="b_primary width_14 py-1"
                    label="Login"
                    onClick={() => {
                      // dispatch(userLogout(history));
                    }}
                    isLoading={false}
                  />
                </Link>
              )}
            </div>
          </div>
        </div>
      </nav>
    </section>
  );
});
