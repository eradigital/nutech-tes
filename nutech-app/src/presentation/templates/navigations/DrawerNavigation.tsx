import React from "react";
import { Fragment } from "react";
import { Link } from "react-router-dom";
import { ROUTES } from "@nutech-core/constants/routes/list_route.constant";

export default React.memo(function DrawerNavigation(props: any) {
  const { translate, dimension } = props;

  return (
    <section
      className={`active sticky-top ${dimension} menu_container`}
      id="side_menu"
    >
      <nav className={`${translate}`}>
        <div className="b_primary_10 px-0 py-5">
          <div style={{ marginLeft: 20, marginRight: 20 }}>
            <img src="/images/icons/logo.png" alt="" className="image-logo" />
          </div>
        </div>
        <div className="px-4 mt-4">
          <Fragment>
            <div
              onClick={() => {}}
              className="b_primary_10 mt-3 py-2 pl-3 rounded-pill d-flex align-items-center justify-content-between mb-4 btn-style size_25 fw-bold"
            >
              <span className="font-weight-bold ms-4 size_40">Produk</span>
            </div>
            <ul className="mt-3">
              <li className="mt-2">
                <Link
                  style={{}}
                  className="text-decoration-none"
                  to={ROUTES.LIST_PRODUCT}
                >
                  <div
                    onClick={() => {}}
                    className="d-flex justify-content-between btn-style"
                  >
                    <span className="size_30">Daftar Produk</span>
                    <i
                      className="fa fa-arrow-right me-3 mb-3"
                      aria-hidden="true"
                    ></i>
                  </div>
                </Link>
              </li>
              <li className="mt-2">
                <Link
                  style={{}}
                  className="text-decoration-none"
                  to={ROUTES.CREATE_PRODUCT}
                >
                  <div
                    onClick={() => {}}
                    className="d-flex justify-content-between btn-style"
                  >
                    <span className="size_30">Tambah Produk</span>
                    <i
                      className="fa fa-arrow-right me-3 mb-3"
                      aria-hidden="true"
                    ></i>
                  </div>
                </Link>
              </li>
            </ul>
          </Fragment>
        </div>
      </nav>
    </section>
  );
});
