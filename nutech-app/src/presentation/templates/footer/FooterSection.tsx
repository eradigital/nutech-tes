import React from "react";

export default React.memo(function FooterSection() {
  return (
    <footer className="mx-5 py-5" id="footer">
      <hr />
      <div className="d-flex justify-content-between mt-5 align-items-end">
        <img src="/images/logo.svg" alt="" className="col-3 px-4" />
        <p>Created By EraDigital @CopyRight 2020</p>
      </div>
    </footer>
  );
});
