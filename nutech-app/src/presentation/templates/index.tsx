import React, { useState } from "react";
import HeaderNavigation from "./navigations/HeaderNavigation";
import DrawerNavigation from "./navigations/DrawerNavigation";
import FooterSection from "./footer/FooterSection";
import { useParams } from "react-router-dom";

export default React.memo(function TemplatePage(props: any) {
  const [translate, setTranslate] = useState("translateRight");
  const [dimension, setDimension] = useState("zoomIn");
  const params: any = useParams();

  const toogleDrawer = () => {
    if (translate === "translateRight") {
      setTranslate("translateLeft");
      setDimension("zoomOut");
    } else {
      setTranslate("translateRight");
      setDimension("zoomIn");
    }
  };

  return (
    <main className="reset col-md-12" id="home_page">
      {params?.action !== "create" && (
        <DrawerNavigation translate={translate} dimension={dimension} />
      )}
      <main className="w-100">
        <HeaderNavigation
          translate={translate}
          toogleDrawer={toogleDrawer}
          action={params?.action}
        />
        <div className="wrapper_child">{props.children}</div>
        <FooterSection />
      </main>
    </main>
  );
});
