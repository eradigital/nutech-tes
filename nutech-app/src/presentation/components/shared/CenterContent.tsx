import React from "react";

function CenterContent(props: any) {
  return (
    <div className="d-flex flex-column justify-content-center align-items-center">
        {props.children}
    </div>
  );
}

export default React.memo(CenterContent);
