import React from "react";

function CenterRow(props: any) {
  return (
    <div className="d-flex justify-content-center align-items-center">
        {props.children}
    </div>
  );
}

export default React.memo(CenterRow);
