import React from "react";

export default React.memo(function ComponentTitle(props: any) {
  const { title, desc } = props;
  return (
    <div className="mt-4">
      <h3 className="t-title reset pb-3">{title}</h3>
      <span className="reset">{desc}</span>
      <hr className="hr reset mt-1" />
      <br />
    </div>
  );
});
