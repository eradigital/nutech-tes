import React from "react";

export default React.memo(function ComponentCircleButton(props: any) {
  const { onClick, icon, className } = props;

  return (
    <div
      {...props}
      onClick={onClick}
      className={`${className} main_menu d-flex justify-content-center align-items-center btn-style`}
    >
      <i className={`${icon} c_white`} aria-hidden="true"></i>
    </div>
  );
});
