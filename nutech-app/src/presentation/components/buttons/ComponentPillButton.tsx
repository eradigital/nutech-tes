import React, { Fragment } from "react";
import "react-loader-spinner/dist/loader/css/react-spinner-loader.css";
import Loader from "react-loader-spinner";

export default React.memo(function ComponentPillButton({
  label,
  onClick,
  className,
  isLoading
}: {
  label: string;
  onClick: any;
  className: string;
  isLoading: boolean;
}) {
  return (
    <Fragment>
      {!isLoading && (
        <div
          className={`text-white rounded-pill px-4 btn-text shadow_primary ${className} btn-style`}
          onClick={onClick}
        >
          {label}
        </div>
      )}
      {isLoading && (
        <Loader type="Puff" color="#00BFFF" height={30} width={30} />
      )}
    </Fragment>
  );
})
