import "date-fns";
import React, { useEffect } from "react";
import DateFnsUtils from "@date-io/date-fns";
import {
  MuiPickersUtilsProvider,
  KeyboardTimePicker
} from "@material-ui/pickers";

export default React.memo(function ComponentTimePicker(props: any) {
  const { initialTime, onChanged } = props;
  const [selectedDate, setSelectedDate] = React.useState(new Date(initialTime));

  const handleDateChange = (date: any) => {
    setSelectedDate(date);
    onChanged(date);
  };

  useEffect(() => {
    setSelectedDate(initialTime);
    return () => {};
  }, [initialTime]);

  return (
    <MuiPickersUtilsProvider utils={DateFnsUtils}>
      <KeyboardTimePicker
        margin="normal"
        id="time-picker"
        label="Time picker"
        value={selectedDate}
        onChange={handleDateChange}
        KeyboardButtonProps={{
          "aria-label": "change time"
        }}
      />
    </MuiPickersUtilsProvider>
  );
});
