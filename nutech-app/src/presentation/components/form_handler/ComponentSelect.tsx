import React, { Fragment, useEffect } from "react";
import { onChangeFormHandling } from "@nutech-application/redux/form_handling/actions/onchange.action";
import { initializeFormHandling } from "@nutech-application/redux/form_handling/actions/initialize.action";
import validation from "@nutech-core/utils/validation";
import { useSelector, useDispatch } from "react-redux";

export default React.memo(function ComponentSelect({
  data_select = [],
  showIcon = false,
  label,
  message,
  name,
  onChangeText,
  initialValue = 0,
  form,
  required,
  max,
  min,
  email
}: {
  data_select: any;
  showIcon?: boolean;
  label: string;
  message: string;
  name: string;
  onChangeText: Function;
  initialValue?: number | undefined;
  form: string;
  required?: any;
  max?: number;
  min?: number;
  email?: string;
}) {
  const dispatch = useDispatch();
  const stateRedux: any = useSelector((state) => state);
  const stateValidation = stateRedux.form_handling;

  useEffect(() => {
    const initializeInput = async () => {
      let isValid = await validation(initialValue, required, max, min, email);
      let data = {
        form: form ? form : "",
        value: initialValue,
        isValid: isValid,
        id: name
      };
      dispatch(initializeFormHandling(data));
    };
    initializeInput();
  }, [dispatch, initialValue, name, required, email, max, min, form]);

  const textChangeHandler = async (text: any) => {
    let isValid = await validation(text, required, max, min, email);
    let data = {
      form: form ? form : "",
      value: text,
      isValid: isValid,
      id: name
    };
    onChangeText(text, isValid);
    dispatch(onChangeFormHandling(data));
  };

  let stateInput = stateValidation.input.filter((el: any) => el.name === name);

  return (
    <Fragment>
      <div
        className="d-flex justify-content-end align-items-end col-12"
        id="select-input"
      >
        <div className="form me-3">
          <select
            value={initialValue * 1}
            onChange={(text) => {
              textChangeHandler(text.target.value);
            }}
            className="browser-default custom-select"
          >
            <option value="">{label}</option>
            {data_select.map((item: any) => {
              return (
                <option key={item.id} value={item.id}>
                  {item.name}
                </option>
              );
            })}
          </select>
          <label htmlFor="name" className="label-name ">
            <span className="content-name" />
          </label>
        </div>
        {showIcon && (
          <a href="/" className="btn-eye">
            <i className="fas fa-eye"></i>
          </a>
        )}
      </div>
      {stateValidation.isSubmit && !stateInput[0]?.isValid && (
        <div className="error-msg">{message}</div>
      )}
    </Fragment>
  );
});
