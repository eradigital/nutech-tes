import React, { useEffect } from "react";
import { currency } from "@nutech-core/utils/currency.util";
import { useSelector, useDispatch } from "react-redux";
import validation from "@nutech-core/utils/validation";
import { initializeFormHandling } from "@nutech-application/redux/form_handling/actions/initialize.action";
import { onChangeFormHandling } from "@nutech-application/redux/form_handling/actions/onchange.action";

const ComponentCurrency: React.FC<{
  message: string;
  label: string;
  name: string;
  showIcon?: boolean;
  onChangeText: Function;
  initialValue?: string | number | undefined;
  form: string;
  required?: any;
  max?: number;
  min?: number;
  email?: string;
}> = React.memo((props) => {
  const {
    message,
    label,
    name,
    showIcon = false,
    onChangeText,
    initialValue = "",
    form,
    required,
    max,
    min,
    email
  } = props;
  const dispatch = useDispatch();
  const stateRedux: any = useSelector((state) => state);
  const stateValidation = stateRedux.form_handling;

  useEffect(() => {
    const initializeInput = async () => {
      let isValid = await validation(initialValue, required, max, min, email);
      let data = {
        form: form ? form : "",
        value: initialValue,
        isValid: isValid,
        id: name
      };
      dispatch(initializeFormHandling(data));
    };
    initializeInput();
  }, [dispatch, initialValue, name, required, email, max, min, form]);

  const textChangeHandler = async (text: any) => {
    let isValid = await validation(text, required, max, min, email);
    let data = {
      form: form ? form : "",
      value: text,
      isValid: isValid,
      id: name
    };
    onChangeText(text, isValid);
    dispatch(onChangeFormHandling(data));
  };

  let stateInput = stateValidation.input.filter((el: any) => el.name === name);

  return (
    <div>
      <ComponentCurrencyInput
        initialValue={initialValue.toString()}
        showIcon={showIcon}
        label={label}
        onChangeInput={textChangeHandler}
      />
      {stateValidation.isSubmit && !stateInput[0]?.isValid && (
        <div className="error-msg">{message}</div>
      )}
    </div>
  );
});

export default ComponentCurrency;

class ComponentCurrencyInput extends React.Component<
  {
    showIcon: boolean;
    label: string;
    onChangeInput: (value: string) => void;
    initialValue: string;
  },
  { text: string }
> {
  private input: React.RefObject<HTMLInputElement>;
  constructor(props: any) {
    super(props);
    this.state = {
      text: this.props.initialValue
        ? currency(parseInt(this.props.initialValue))
        : "Rp 0"
    };
    this.input = React.createRef();
  }

  handleChange(e: React.ChangeEvent<HTMLInputElement>) {
    const cursor: any = e.target.selectionStart;
    let removeDotSymbol = e.target.value.replaceAll(".", "");
    let numberOfPrice = removeDotSymbol.replace(/[^\d.-]/g, "");
    let strCurrency = numberOfPrice.length > 0 ? numberOfPrice : "0";
    let price: string = currency(parseInt(strCurrency));
    this.props.onChangeInput(numberOfPrice);
    if (cursor <= e.target.value) {
      this.setState({ text: price }, () => {
        if (this.input.current != null)
          this.input.current.selectionEnd = cursor - 2;
      });
    } else {
      this.setState({ text: price }, () => {
        if (this.input.current != null)
          this.input.current.selectionEnd = cursor + 1;
      });
    }
  }

  render() {
    return (
      <React.Fragment>
        <div
          className="d-flex justify-content-end align-items-end"
          id="basic-input"
        >
          <div className="form me-3">
            <input
              autoComplete="off"
              ref={this.input}
              type="text"
              required
              value={this.state.text}
              onChange={this.handleChange.bind(this)}
            />
            <label htmlFor="name" className="label-name ">
              <span className="content-name">{this.props.label}</span>
            </label>
          </div>
          {this.props.showIcon && (
            <a href="/" className="btn-eye">
              <i className="fas fa-eye"></i>
            </a>
          )}
        </div>
      </React.Fragment>
    );
  }
}
