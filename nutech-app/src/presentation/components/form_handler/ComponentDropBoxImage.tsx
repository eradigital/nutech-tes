import React, { useRef, useState, useEffect } from "react";
import axios from "axios";
import { useSelector } from "react-redux";
import alert_notification from "@nutech-core/utils/alert_notification.util";
import Swal from "sweetalert2";
const baseUrl = process.env.REACT_APP_PUBLIC_URL;

export default React.memo(function ComponentDropBoxImage(props: any) {
  const { onChange, imageUrl } = props;
  const stateRedux: any = useSelector((state) => state);
  const stateAuth = stateRedux.auth.posts;
  const token = stateAuth ? stateAuth.token.access_token : "";
  const [image, setImage] = useState(imageUrl);
  const hiddenFileInput: any = useRef();

  useEffect(() => {
    setImage(imageUrl);
    return () => {};
  }, [imageUrl]);

  const dragOver = (e: any) => {
    e.preventDefault();
    console.log("dragOver");
  };

  const dragEnter = (e: any) => {
    e.preventDefault();
    console.log("dragEnter");
  };

  const dragLeave = (e: any) => {
    e.preventDefault();
    console.log("dragLeave");
  };

  const fileDrop = async (e: any) => {
    e.preventDefault();
    const files = e.dataTransfer.files;
    handleFiles(files);
  };

  const handleChange = (e: any) => {
    const fileUploaded = e.target.files[0];
    uploadFile(fileUploaded);
  };

  const handleFiles = async (files: any) => {
    [...files].forEach(uploadFile);
  };

  const uploadFile = async (file: any) => {
    let url = "image/upload";
    let formData = new FormData();
    if (file.type === "image/jpeg" || file.type === "image/png") {
      if (file.size > 100000) {
        Swal.fire(
          alert_notification({
            title: "Info",
            message: "Ukuran gambar harus di bawah 100KB",
            status: "info",
            type: "default",
            position: "center"
          })
        );
      } else {
        try {
          formData.append("image", file);
          const response: any = await axios({
            method: "POST",
            url: baseUrl + "/" + url,
            data: formData,
            headers: {
              Accept: "application/json",
              "content-type": "multipart/form-data",
              Authorization: `Bearer ${token}`
            }
          });
          const dataImage = response.data;
          if (dataImage.success) {
            setImage(dataImage.data.file_url);
            onChange(dataImage.data);
          } else {
            Swal.fire(
              alert_notification({
                title: "Info",
                message: response.message,
                status: "info",
                type: "default",
                position: "center"
              })
            );
          }
        } catch (error) {
          console.log(
            "Terjadi kesalahan dari sisi server dengan status (unhandle exeption) dari endpoint : " +
              baseUrl +
              "/" +
              url +
              " dengan error " +
              error
          );
        }
      }
    } else {
      Swal.fire(
        alert_notification({
          title: "Info",
          message: "Format gambar harus png atau jpg/jpeg",
          status: "info",
          type: "default",
          position: "center"
        })
      );
    }
  };

  const handleClick = () => {
    hiddenFileInput.current.click();
  };

  const dropImage = {
    backgroundImage: `url('${image}')`,
    backgroundPosition: "center",
    backgroundSize: "contain",
    backgroundRepeat: "no-repeat"
  };

  return (
    <div className="col-md-12 reset mt-5 row">
      <div className="col-6 d-flex flex-column justify-content-center align-items-center reset pe-2">
        <div
          className="border col-12 d-flex flex-column justify-content-center align-items-center h-100"
          style={dropImage}
        >
          <strong>PREVIEW</strong>
        </div>
      </div>
      <div
        className="col-6 p-5 d-flex flex-column justify-content-center align-items-center"
        id="drop-box"
        onDragOver={dragOver}
        onDragEnter={dragEnter}
        onDragLeave={dragLeave}
        onDrop={fileDrop}
      >
        <img src="/images/upload.svg" width="30%" alt="" />
        <br />
        <input
          type="file"
          ref={hiddenFileInput}
          onChange={handleChange}
          style={{ display: "none" }}
        />
        <div>
          <strong>DROP IMAGE HERE</strong>
        </div>
        <div className="py-3">
          <span>OR</span>
        </div>
        <div className="btn-style btn btn-primary" onClick={handleClick}>
          SELECT IMAGE
        </div>
      </div>
    </div>
  );
})
