import React, { Fragment, useEffect } from "react";
import { onChangeFormHandling } from "@nutech-application/redux/form_handling/actions/onchange.action";
import { initializeFormHandling } from "@nutech-application/redux/form_handling/actions/initialize.action";
import validation from "@nutech-core/utils/validation";
import PropTypes from "prop-types";
import { useSelector, useDispatch } from "react-redux";

export default React.memo(function ComponentTextArea(props: any) {
  const {
    message,
    label,
    name,
    showIcon = false,
    value,
    onChangeText,
    initialValue = "",
    form,
    placeholder
  } = props;
  const { required, max, min, email } = props;
  const dispatch = useDispatch();
  const stateRedux: any = useSelector((state) => state);
  const stateValidation = stateRedux.form_handling;

  useEffect(() => {
    const initializeInput = async () => {
      let isValid = await validation(initialValue, required, max, min, email);
      let data = {
        form: form ? form : "",
        value: initialValue,
        isValid: isValid,
        id: name
      };
      dispatch(initializeFormHandling(data));
    };
    initializeInput();
  }, [dispatch, initialValue, name, required, email, max, min, form]);

  const textChangeHandler = async (text: any) => {
    let isValid = await validation(text, required, max, min, email);
    let data = {
      form: form ? form : "",
      value: text,
      isValid: isValid,
      id: name
    };
    onChangeText(text, isValid);
    dispatch(onChangeFormHandling(data));
  };

  let stateInput = stateValidation.input.filter((el: any) => el.name === name);

  return (
    <Fragment>
      <div
        className="d-flex justify-content-end align-items-end"
        id="textarea-input"
      >
        <div className="form me-0 reset">
          <textarea
            className="reset"
            placeholder={placeholder}
            required
            name={name}
            value={value}
            onChange={(text) => {
              textChangeHandler(text.target.value);
            }}
          />
          <label htmlFor="name" className="label-name ">
            <span className="content-name">{label}</span>
          </label>
        </div>
        {showIcon && (
          <a href="/" className="btn-eye">
            <i className="fas fa-eye"></i>
          </a>
        )}
      </div>
      {stateValidation.isSubmit && !stateInput[0]?.isValid && (
        <div className="error-msg">{message}</div>
      )}
    </Fragment>
  );
});
