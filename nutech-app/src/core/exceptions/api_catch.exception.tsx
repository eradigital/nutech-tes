import { info_message } from "@nutech-core/utils/alert_notification.util";
import { LocalStorage } from "@nutech-core/services/local_storage.service";

export const apiCatchException = async (
  error: any,
  message: string = "Maaf server sedang mengalami gangguan silahkan coba lagi beberapa saat nanti"
) => {
  const status = error.response.status;
  console.log(error.response, status === 401);
  
  if (status >= 100 && status < 200) {
    let msg =
      "Permintaan anda tidak dapat di proses saat ini, silahkan coba lagi beberapa saat";
    info_message(msg);
  } else if (status >= 300 && status < 400) {
    let msg = "Permintaan anda tidak dapat di proses";
    info_message(msg);
  } else if (status === 401) {
    await LocalStorage.clearStorage();
    let msg = "Silahkan login terlebih dahulu untuk dapat mengakses halaman";
    info_message(msg);
  } else if (status !== 401 && status >= 400 && status < 500) {
    let msg =
      "Permintaan anda tidak dapat di proses saat ini, silahkan coba lagi beberapa saat";
    info_message(msg);
  } else if (status >= 500 && status < 600) {
    let msg =
      "Layanan kami sedang dalam perbaikan, mohon maaf saat ini tidak dapat di akses";
    info_message(msg);
  } else {
    info_message(message);
  }

  return error.response.data;
};
