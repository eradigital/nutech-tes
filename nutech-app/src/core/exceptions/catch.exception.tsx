import { info_message } from "@nutech-core/utils/alert_notification.util";

export const catchException = (
  error: any,
  message: string = "Maaf server sedang mengalami gangguan silahkan coba lagi beberapa saat nanti"
) => {
  console.log(error);
  info_message(message);
};
