import { info_message } from "@nutech-core/utils/alert_notification.util";

export const defaultException = (
  showAlert: boolean = false,
  message: string = "Maaf server sedang mengalami gangguan silahkan coba lagi beberapa saat nanti"
) => {
  if(showAlert){
    info_message(message);
  }
};
