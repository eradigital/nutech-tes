import { reactLocalStorage } from "reactjs-localstorage";

export class LocalStorage {
  static setDataStorage = async (
    location: string,
    data: string | number | boolean
  ) => await reactLocalStorage.set(location, data);

  static getDataStorage = async (
    location: string,
    defaultValue: string | number | boolean | undefined
  ) => await reactLocalStorage.get(location, defaultValue);

  static setDataObjectStorage = async (location: string, data: object) =>
    await reactLocalStorage.setObject(location, data);

  static getDataObjectStorage = async (location: string) =>
    await reactLocalStorage.getObject(location);

  static removeStorage = async (location: string) =>
    await reactLocalStorage.remove(location);

  static clearStorage = async () => await reactLocalStorage.clear();
}
