import axios from "axios";
import { apiCatchException } from "@nutech-core/exceptions/api_catch.exception";
import { LocalStorage } from "@nutech-core/services/local_storage.service";
import { STORAGE } from "@nutech-core/constants/storage/local.storage";

async function auth() {
  let data: any = await LocalStorage.getDataObjectStorage(STORAGE.AUTH);
  return data;
}

const baseUrl = process.env.REACT_APP_PUBLIC_URL;

export async function postApi(config: any) {
  let key = await auth();
  let token = Object.keys(key).length > 0 ? key.token.access_token : "";
  let data = config.data ? config.data : {};
  let url = config.url ? config.url : "";
  try {
    const response = await axios({
      method: "POST",
      url: baseUrl + "/" + url,
      data: data,
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
        Authorization: `Bearer ${token}`
      }
    });
    return response.data;
  } catch (error) {
    return apiCatchException(error);
  }
}

export async function getApi(config: any) {
  let key = await auth();
  let token = Object.keys(key).length > 0 ? key.token.access_token : "";
  let data = config.data ? config.data : {};
  let url = config.url ? config.url : "";
  try {
    const response = await axios({
      method: "GET",
      url: baseUrl + "/" + url,
      params: data,
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
        Authorization: `Bearer ${token}`
      }
    });
    return response.data;
  } catch (error) {
    return apiCatchException(error);
  }
}

export async function putApi(config: any) {
  let key = await auth();
  let token = Object.keys(key).length > 0 ? key.token.access_token : "";
  let data = config.data ? config.data : {};
  let url = config.url ? config.url : "";
  try {
    const response = await axios({
      method: "PUT",
      url: baseUrl + "/" + url,
      data: data,
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
        Authorization: `Bearer ${token}`
      }
    });
    return response.data;
  } catch (error) {
    return apiCatchException(error);
  }
}

export async function deleteApi(config: any) {
  let key = await auth();
  let token = Object.keys(key).length > 0 ? key.token.access_token : "";
  let data = config.data ? config.data : {};
  let url = config.url ? config.url : "";
  try {
    const response = await axios({
      method: "DELETE",
      url: baseUrl + "/" + url,
      data: data,
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
        Authorization: `Bearer ${token}`
      }
    });
    return response.data;
  } catch (error) {
    return apiCatchException(error);
  }
}
