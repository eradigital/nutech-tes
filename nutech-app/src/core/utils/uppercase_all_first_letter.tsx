function toUpperCaseAllWord(str: string) 
{
    let stringArray = str.split(" ");

    for (var i = 0, x = stringArray.length; i < x; i++) {
        stringArray[i] = stringArray[i][0].toUpperCase() + stringArray[i].substr(1);
    }

    return stringArray.join(" ");
}

export default toUpperCaseAllWord;