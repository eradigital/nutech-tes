export default function validation(text:string|number|undefined = "", required: any, max: any, min: any, email: any) {
  const emailRegex =/^[a-zA-Z0-9]+@[a-zA-Z0-9]+\.[A-Za-z]+$/;
  let isValid = true;

  if (required && text.toString().trim().length === 0) {
    return (isValid = false);
  }
  if (email && !emailRegex.test(text.toString().toLowerCase())) {
    return (isValid = false);
  }
  if (min != null && +text < min) {
    return (isValid = false);
  }
  if (max != null && +text > max) {
    return (isValid = false);
  }

  return isValid;
}
