export enum ROUTES {
  SIGNIN = "/",
  CREATE_PRODUCT = "/create_product",
  LIST_PRODUCT = "/list_product",
  UPDATE_PRODUCT = "/update_product",
}
