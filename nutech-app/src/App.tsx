import React, { useEffect } from "react";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import PrivateRoute from "@nutech-presentation/routes/PrivateRoute";
import { useSelector, useDispatch } from "react-redux";
import { reactLocalStorage } from "reactjs-localstorage";
import { restoreToken } from "@nutech-application/redux/auth/middlewares/restoreToken.middleware";

import SignInPage from "@nutech-presentation/pages/signin/SignIn.page";
import ListProductPage from "@nutech-presentation/pages/product/list_product/ListProduct.page";

export default function App() {
  const dispatch = useDispatch();
  const auth: any = reactLocalStorage.getObject("auth");
  const stateRedux: any = useSelector((state) => state);
  const stateAuth = auth.token ? auth : stateRedux.auth.posts;

  useEffect(() => {
    if (auth.token) {
      dispatch(restoreToken(auth));
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return (
    <Router>
      <Switch>
        <Route path="/" exact component={SignInPage} />
        <PrivateRoute
          authed={stateAuth}
          path="/list_product"
          exact
          component={ListProductPage}
        />
      </Switch>
    </Router>
  );
}
