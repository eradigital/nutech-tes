export declare module LoginResponseInterface {

    export interface Token {
        token_type: string;
        expires_in: number;
        access_token: string;
        refresh_token: string;
    }

    export interface UserData {
        id: number;
        email: string;
        username: string;
        verified: string;
        verification_token: string;
        role_user: string;
        created_at: Date;
        updated_at: Date;
    }

    export interface Data {
        token: Token;
        user_data: UserData;
    }

    export interface RootObject {
        success: boolean;
        message: string;
        data: Data;
    }

}


  
  export class LoginResponseModel {
    static toJson(event: LoginResponseInterface.RootObject) {
      return {
        success: event.success,
        message: event.message,
        data: event.data
      };
    }
  
    static fromJson(response: any) {
      return {
        success: response.success,
        message: response.message,
        data: response.data
      };
    }
  }
  