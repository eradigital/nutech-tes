export declare module LoginRequestInterface {

    export interface RootObject {
        email: string;
        password: string;
    }

}
  
  export class LoginRequestModel {
    static toJson(event: LoginRequestInterface.RootObject) {
      return {
        email: event.email,
        password: event.password,
      };
    }
  
    static fromJson(response: any) {
      return {
        email: response.email,
        password: response.password,
      };
    }
  }
  