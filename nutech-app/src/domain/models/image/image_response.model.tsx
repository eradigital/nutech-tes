export declare module ImageResponseInterface {
  export interface Data {
    name: string;
    file_local: string;
    file_url: string;
    file_extension: string;
    file_size: number;
    updated_at: Date;
    created_at: Date;
    id: number;
  }

  export interface RootObject {
    success: boolean;
    message: string;
    data: Data;
  }
}

export class ImageResponseModel {
  static toJson(event: ImageResponseInterface.RootObject) {
    return {
      success: event.success,
      message: event.message,
      data: event.data
    };
  }

  static fromJson(response: any) {
    return {
      success: response.success,
      message: response.message,
      data: response.data
    };
  }
}
