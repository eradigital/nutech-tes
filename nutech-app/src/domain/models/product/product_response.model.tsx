export declare module ProductResponseInterface {
  export interface Data {
    file_image_id: string;
    name_product: string;
    purchase_price: string;
    selling_price: string;
    stock: string;
    updated_at: Date;
    created_at: Date;
    id: number;
  }

  export interface RootObject {
    success: boolean;
    message: string;
    data: Data;
  }
}

export class ProductResponseModel {
  static toJson(event: ProductResponseInterface.RootObject) {
    return {
      success: event.success,
      message: event.message,
      data: event.data
    };
  }

  static fromJson(response: any) {
    return {
      success: response.success,
      message: response.message,
      data: response.data
    };
  }
}

