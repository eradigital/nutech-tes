export declare module ListProductResponseInterface {
  export interface Datum {
    id: number;
    file_image_id: number;
    name_product: string;
    purchase_price: number;
    selling_price: number;
    stock: number;
    created_at: Date;
    updated_at: Date;
  }

  export interface Link {
    url: string;
    label: string;
    active: boolean;
  }

  export interface Data {
    current_page: number;
    data: Datum[];
    first_page_url: string;
    from: number;
    last_page: number;
    last_page_url: string;
    links: Link[];
    next_page_url: string;
    path: string;
    per_page: number;
    prev_page_url: string;
    to: number;
    total: number;
  }

  export interface RootObject {
    success: boolean;
    message: string;
    data: Data;
  }
}

export class ProductResponseModel {
  static toJson(event: ListProductResponseInterface.RootObject) {
    return {
      success: event.success,
      message: event.message,
      data: event.data
    };
  }

  static fromJson(response: any) {
    return {
      success: response.success,
      message: response.message,
      data: response.data
    };
  }
}
