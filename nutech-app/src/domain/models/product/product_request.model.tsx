export declare module ProductRequestInterface {
  export interface RootObject {
    file_image_id: number;
    name_product: string;
    purchase_price: number;
    selling_price: number;
    stock: number;
  }
}

export class ProductRequestModel {
  static toJson(event: ProductRequestInterface.RootObject) {
    return {
      file_image_id: event.file_image_id,
      name_product: event.name_product,
      purchase_price: event.purchase_price,
      selling_price: event.selling_price,
      stock: event.stock
    };
  }

  static fromJson(response: any) {
    return {
      file_image_id: response.file_image_id,
      name_product: response.name_product,
      purchase_price: response.purchase_price,
      selling_price: response.selling_price,
      stock: response.stock
    };
  }
}
