import { ImageRequestInterface } from "@nutech-domain/models/image/image_request.model";
import { ImageResponseInterface } from "@nutech-domain/models/image/image_response.model";
import { uploadImage } from "@nutech-infrastructure/services/image/upload_image.service";

export const uploadImageRepository = async (data: typeof ImageRequestInterface) => {
  const result: ImageResponseInterface.RootObject = await uploadImage(data);
  return result;
};
