import { ImageResponseInterface } from "@nutech-domain/models/image/image_response.model";
import { deleteImage } from "@nutech-infrastructure/services/image/delete_image.service";

export const deleteProductRepository = async (id: string) => {
  const result: ImageResponseInterface.RootObject = await deleteImage(id);
  return result;
};
