import { LogoutResponseInterface } from "@nutech-domain/models/auth/logout_response.model";
import { logout } from "@nutech-infrastructure/services/auth/logout.service";

export const logoutRepository = async () => {
  const result: LogoutResponseInterface.RootObject = await logout();
  return result;
};
