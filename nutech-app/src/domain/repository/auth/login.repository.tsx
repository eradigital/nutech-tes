import { LoginResponseInterface } from "@nutech-domain/models/auth/auth_response.model";
import { LoginRequestInterface } from "@nutech-domain/models/auth/auth_request.model";
import { login } from '@nutech-infrastructure/services/auth/login.service';

export const loginRepository = async (data: LoginRequestInterface.RootObject) => {
  const result: LoginResponseInterface.RootObject = await login(data);
  return result;
};
