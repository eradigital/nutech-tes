import { ProductResponseInterface } from "@nutech-domain/models/product/product_response.model";
import { getDetailProduct } from "@nutech-infrastructure/services/product/detail_product.service";

export const getDetailProductRepository = async (id: string) => {
  const result: ProductResponseInterface.RootObject = await getDetailProduct(id);
  return result;
};
