import { ProductRequestInterface } from "@nutech-domain/models/product/product_request.model";
import { ProductResponseInterface } from "@nutech-domain/models/product/product_response.model";
import { updateProduct } from "@nutech-infrastructure/services/product/update_product.service";

export const updateProductRepository = async (
  id: string,
  data: ProductRequestInterface.RootObject
) => {
  const result: ProductResponseInterface.RootObject = await updateProduct(
    id,
    data
  );
  return result;
};
