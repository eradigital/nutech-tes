import { ProductRequestInterface } from "@nutech-domain/models/product/product_request.model";
import { ProductResponseInterface } from "@nutech-domain/models/product/product_response.model";
import { createProduct } from "@nutech-infrastructure/services/product/create_product.service";

export const createProductRepository = async (data: ProductRequestInterface.RootObject) => {
  const result: ProductResponseInterface.RootObject = await createProduct(data);
  return result;
};
