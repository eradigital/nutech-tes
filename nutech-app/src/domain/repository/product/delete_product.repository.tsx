import { ProductResponseInterface } from "@nutech-domain/models/product/product_response.model";
import { deleteProduct } from "@nutech-infrastructure/services/product/delete_product.service";

export const deleteProductRepository = async (id: string) => {
  const result: ProductResponseInterface.RootObject = await deleteProduct(id);
  return result;
};
