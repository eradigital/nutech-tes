import { ListProductResponseInterface } from "@nutech-domain/models/product/list_product_response.model";
import { getProduct } from "@nutech-infrastructure/services/product/get_product.service";

export const getProductRepository = async (page: string, keyword: string) => {
  const result: ListProductResponseInterface.RootObject = await getProduct(page, keyword);
  return result;
};
