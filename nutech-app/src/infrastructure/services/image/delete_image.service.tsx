import { deleteApi } from "@nutech-core/services/api.service";

export const deleteImage = async (id: string) => {
  const config = {
    url: "image/delete/" + id,
    data: {}
  };
  const response = await deleteApi(config);
  return response;
};
