import { postApi } from "@nutech-core/services/api.service";

export const logout = async () => {
  const config = {
    url: "user/logout",
    data: {}
  };
  const response = await postApi(config);
  return response;
};
