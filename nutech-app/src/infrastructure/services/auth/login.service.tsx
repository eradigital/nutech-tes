import { postApi } from "@nutech-core/services/api.service";

export const login = async (data: any) => {
  const config = {
    url: "user/login",
    data: data
  };
  const response = await postApi(config);
  return response;
};
