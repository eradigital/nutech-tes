import { deleteApi } from "@nutech-core/services/api.service";

export const deleteProduct = async (id: string) => {
    const config = {
        url: 'product/' + id,
        data: {},
    }
    const response = await deleteApi(config);
    return response;
};
