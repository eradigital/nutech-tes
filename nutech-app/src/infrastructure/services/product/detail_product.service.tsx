import { getApi } from "@nutech-core/services/api.service";

export const getDetailProduct = async (id: string) => {
    const config = {
        url: 'product/' + id,
        data: {},
    }
    const response = await getApi(config);
    return response;
};
