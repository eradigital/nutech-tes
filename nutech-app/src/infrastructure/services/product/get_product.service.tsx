import { getApi } from "@nutech-core/services/api.service";

export const getProduct = async (page: string, keyword: string) => {
    const config = {
        url: 'product?page=' + page + '&keyword=' + keyword,
        data: {},
    }
    const response = await getApi(config);
    return response;
};
