import { putApi } from "@nutech-core/services/api.service";

export const updateProduct = async (id: string, data: any) => {
    const config = {
        url: 'product/' + id,
        data: data,
    }
    const response = await putApi(config);
    return response;
};
