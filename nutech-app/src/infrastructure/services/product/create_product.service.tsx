import { postApi } from "@nutech-core/services/api.service";
import { info_message } from "@nutech-core/utils/alert_notification.util";

export const createProduct = async (data: any) => {
  const config = {
    url: "product",
    data: data
  };
  const response = await postApi(config);
  if (response.success) {
    return response;
  } else {
    let msg = response.data.original.errors.name_product[0];
    if (msg === "The name product has already been taken.") {
      info_message(
        "Nama produk sudah di gunakan harap gunakan nama produk yang lain"
      );
    }
  }
};
