import { render } from "react-dom";
import { Provider } from "react-redux";
import App from "@nutech-presentation/routes/App";
import store from "@nutech-application/redux";

const renderApp = () =>
  render(
    <Provider store={store}>
      <App />
    </Provider>,
    document.getElementById("root")
  );

renderApp();
