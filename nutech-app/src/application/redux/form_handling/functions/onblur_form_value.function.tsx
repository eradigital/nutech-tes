import {
  InputValue,
  FormInput,
} from '@nutech-application/redux/form_handling/models/form_handling.model';

export const onblurFormValues = (data: any, inputState: any) => {
  let unShift: any = inputState.input.filter((el: any) => el.name !== data.id);
  let selfInput: any = inputState.input.filter(
    (el: any) => el.name === data.id,
  );

  let _name: string = data.id;
  let _isValid: boolean = selfInput[0].isValid;
  let _inputValues: any = selfInput[0].inputValues;
  let _form: string = inputState.form;

  const newInput: InputValue = {
    name: _name,
    isValid: _isValid,
    value: _inputValues,
    touched: true,
  };

  let input = [...unShift, newInput];

  let checkFormValid = input.filter((el) => el.isValid === false);
  let _formIsValid: boolean = checkFormValid.length === 0;

  let newState: FormInput = {
    form: _form,
    isSubmit: false,
    formIsValid: _formIsValid,
    input: input,
  };

  return newState;
};
