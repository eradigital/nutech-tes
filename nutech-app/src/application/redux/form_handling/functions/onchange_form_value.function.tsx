import {
  InputValue,
  FormInput,
} from '@nutech-application/redux/form_handling/models/form_handling.model';

export const onchangeFormValues = (data: any, inputState: any) => {
  let unShift = inputState.input.filter((el: any) => el.name !== data.id);
  const newInput: InputValue = {
    name: data.id,
    isValid: data.isValid,
    value: data.value,
    touched: true,
  };
  let input = [...unShift, newInput];
  let checkFormValid = input.filter((el) => el.isValid === false);
  let newState: FormInput = {
    form: inputState.form,
    isSubmit: false,
    formIsValid: checkFormValid.length === 0,
    input: input,
  };

  return newState;
};
