import {
  InputValue,
  FormInput,
} from '@nutech-application/redux/form_handling/models/form_handling.model';

export const initFormValues = (
  data: any,
  inputState: any,
  isSameFormEmptyValue: boolean,
  selfInputValue: any,
) => {
  let unShift = inputState.filter((el: any) => el.name !== data.id);
  const newInput: InputValue = {
    name: data.id,
    isValid: data.isValid,
    value: isSameFormEmptyValue ? selfInputValue : data.value,
    touched: false,
  };
  let input = [...unShift, newInput];
  let checkFormValid = input.filter((el) => el.isValid === false);
  let newState: FormInput = {
    form: data.form,
    isSubmit: false,
    formIsValid: checkFormValid.length === 0,
    input: input,
  };

  return newState;
};
