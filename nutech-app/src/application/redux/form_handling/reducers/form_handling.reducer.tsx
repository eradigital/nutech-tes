import {INPUT_VALIDATION} from '@nutech-application/redux/form_handling/constants/form_handling.constant';

const initialState = {form: '', formIsValid: false, isSubmit: false, input: []};

const form_handling = (state = initialState, action: any) => {
  switch (action.type) {
    case INPUT_VALIDATION.ONINIT:
      return action.data;
    case INPUT_VALIDATION.ONBLUR:
      return action.data;
    case INPUT_VALIDATION.ONCHANGE:
      return action.data;
    case INPUT_VALIDATION.ONSUBMIT:
      return action.data;
    case INPUT_VALIDATION.CLEAR:
      return initialState;
    default:
      return state;
  }
};

export default form_handling;
