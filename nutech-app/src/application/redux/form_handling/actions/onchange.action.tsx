import {INPUT_VALIDATION} from '@nutech-application/redux/form_handling/constants/form_handling.constant';
import {onchangeFormValues} from '@nutech-application/redux/form_handling/functions/onchange_form_value.function';

export const onChangeFormHandling = (data: any) => {
  return async (dispatch: any, getState: any) => {
    const inputState = getState().form_handling;
    let newState = onchangeFormValues(data, inputState);
    dispatch({type: INPUT_VALIDATION.ONCHANGE, data: newState});
  };
};
