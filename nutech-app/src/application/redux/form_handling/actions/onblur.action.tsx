import {INPUT_VALIDATION} from '@nutech-application/redux/form_handling/constants/form_handling.constant';
import {onblurFormValues} from '@nutech-application/redux/form_handling/functions/onblur_form_value.function';
import {FormInput} from '@nutech-application/redux/form_handling/models/form_handling.model';

export const onBlurFormHandling = (data: any) => {
  return async (dispatch: any, getState: any) => {
    const inputState: any = getState().form_handling;
    let newState: FormInput = onblurFormValues(data, inputState);
    dispatch({type: INPUT_VALIDATION.ONBLUR, data: newState});
  };
};
