import {INPUT_VALIDATION} from '@nutech-application/redux/form_handling/constants/form_handling.constant';
import {FormInput} from '@nutech-application/redux/form_handling/models/form_handling.model';

export const submitFormHandling = () => {
  return async (dispatch: any, getState: any) => {
    const stateFormHandling = getState().form_handling;
    const _input = stateFormHandling.input;
    const _form = stateFormHandling.form;
    const _formIsValid = stateFormHandling.formIsValid;

    let newState: FormInput = {
      form: _form,
      isSubmit: true,
      formIsValid: _formIsValid,
      input: _input,
    };

    dispatch({type: INPUT_VALIDATION.ONSUBMIT, data: newState});
  };
};
