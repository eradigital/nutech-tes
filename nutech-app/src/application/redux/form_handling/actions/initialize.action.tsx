import { INPUT_VALIDATION } from "@nutech-application/redux/form_handling/constants/form_handling.constant";
import {FormInput} from '@nutech-application/redux/form_handling/models/form_handling.model';
import {initFormValues} from '@nutech-application/redux/form_handling/functions/init_form_value.function';

export const initializeFormHandling = (data: any) => {
  return async (dispatch: any, getState: any) => {
    const _stateForm: any = getState().form_handling;
    const inputState: [] = _stateForm.input;
    let isSameForm: boolean = _stateForm.form === data.form;
    let isEmptyValue: boolean = data.value.length === 0;
    let isNotEmptyValue: boolean = data.value.length > 0;
    let isSameFormEmptyValue: boolean = isSameForm && isEmptyValue;
    let isSameFormNotEmptyValue: boolean = isSameForm && isNotEmptyValue;
    let selfInput: any = inputState.filter((el: any) => el.name === data.id);
    let selfInputValue: any =
      selfInput.length > 0 ? selfInput[0].inputValues : '';

    const setValue = () =>
      initFormValues(data, inputState, isSameFormEmptyValue, selfInputValue);

    if (isSameFormEmptyValue) {
      let newState: FormInput = setValue();
      dispatch({type: INPUT_VALIDATION.ONBLUR, data: newState});
    } else if (isSameFormNotEmptyValue) {
      let newState: FormInput = setValue();
      dispatch({type: INPUT_VALIDATION.ONBLUR, data: newState});
    } else {
      dispatch({type: INPUT_VALIDATION.CLEAR});
      let newState: FormInput = setValue();
      dispatch({type: INPUT_VALIDATION.ONBLUR, data: newState});
    }
  };
};
