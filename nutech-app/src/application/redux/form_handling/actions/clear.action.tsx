import { INPUT_VALIDATION } from "@nutech-application/redux/form_handling/constants/form_handling.constant";

export const clearFormHandling = () => {
  return async (dispatch: any, _: any) => {
    dispatch({
      type: INPUT_VALIDATION.CLEAR,
    });
  };
};
