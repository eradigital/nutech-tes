export interface FormInput {
  form: string;
  isSubmit: boolean;
  formIsValid: boolean;
  input: any;
}

export interface InputValue {
  name: string;
  isValid: boolean;
  value: boolean;
  touched: boolean;
}
