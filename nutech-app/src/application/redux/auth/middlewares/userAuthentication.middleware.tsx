import { success_message } from "@nutech-core/utils/alert_notification.util";
import { authLoadSuccess } from "@nutech-application/redux/auth/actions/auth_load_success.action";
import { loginRepository } from "@nutech-domain/repository/auth/login.repository";
import { LoginRequestModel } from "@nutech-domain/models/auth/auth_request.model";
import { AUTH_MESSAGE } from "../constants/auth.constant";
import { STORAGE } from "@nutech-core/constants/storage/local.storage";
import { ROUTES } from "@nutech-core/constants/routes/list_route.constant";
import { authInit } from "@nutech-application/redux/auth/actions/auth_init.action";
import { authFail } from '@nutech-application/redux/auth/actions/auth_fail.action';
import { LocalStorage } from "@nutech-core/services/local_storage.service";

export const userAuthentication = (data: any, history: any) => {
  return async (dispatch: any) => {
    dispatch(authInit);
    try {
      const response = await loginRepository(LoginRequestModel.fromJson(data));

      if (response.success) {
        await LocalStorage.setDataObjectStorage(STORAGE.AUTH, response.data);
        success_message(AUTH_MESSAGE.SUCCESS);
        dispatch(authLoadSuccess(response.data));
        history.push(ROUTES.LIST_PRODUCT);
      }
    } catch (error) {
      console.log(JSON.stringify(error.message));
      dispatch(authFail(error));
    }
  };
};