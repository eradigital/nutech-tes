import { success_message } from "@nutech-core/utils/alert_notification.util";
import { authLoadSuccess } from "@nutech-application/redux/auth/actions/auth_load_success.action";
import { logoutRepository } from "@nutech-domain/repository/auth/logout.repository";
import { AUTH_MESSAGE } from "../constants/auth.constant";
import { STORAGE } from "@nutech-core/constants/storage/local.storage";
import { authInit } from "@nutech-application/redux/auth/actions/auth_init.action";
import { authFail } from "@nutech-application/redux/auth/actions/auth_fail.action";
import { LocalStorage } from "@nutech-core/services/local_storage.service";
import { ROUTES } from "@nutech-core/constants/routes/list_route.constant";

export const userLogout = (history: any) => {
  return async (dispatch: any, _: any) => {
    dispatch(authInit());
    try {
      const response: any = await logoutRepository();
      if (response.success) {
        await LocalStorage.removeStorage(STORAGE.AUTH);
        history.push(ROUTES.SIGNIN);
        success_message(AUTH_MESSAGE.SUCCESS_LOGOUT);
        dispatch(authLoadSuccess(null));
      }
    } catch (error) {
      console.log(JSON.stringify(error.message));
      dispatch(authFail(error));
    }
  };
};
