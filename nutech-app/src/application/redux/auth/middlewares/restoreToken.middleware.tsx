import { authLoadSuccess } from "@nutech-application/redux/auth/actions/auth_load_success.action";
import { authFail } from '@nutech-application/redux/auth/actions/auth_fail.action';

export const restoreToken = (data: any) => {
  return async (dispatch: any) => {
    try {
      dispatch(authLoadSuccess(data));
    } catch (error) {
      console.log(JSON.stringify(error.message));
      dispatch(authFail(error));
    }
  };
};
