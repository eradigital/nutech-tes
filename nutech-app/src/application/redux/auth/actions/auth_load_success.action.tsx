import { AUTH } from "@nutech-application/redux/auth/constants/auth.constant";

export const authLoadSuccess = (payload: any) => ({
  type: AUTH.LOAD_SUCCESS,
  data: payload
});
