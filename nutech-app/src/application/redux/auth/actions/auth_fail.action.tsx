import { AUTH } from "@nutech-application/redux/auth/constants/auth.constant";

export const authFail = (error: any) => ({
  type: AUTH.LOAD_FAIL,
  data: error.message
});
