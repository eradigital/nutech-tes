import { AUTH } from "@nutech-application/redux/auth/constants/auth.constant";

export const authInit = () => ({ type: AUTH.LOAD_INIT });
