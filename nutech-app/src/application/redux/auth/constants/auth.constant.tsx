export const AUTH = {
  LOAD_INIT: "AUTH_LOAD_INIT",
  LOAD_SUCCESS: "AUTH_LOAD_SUCCESS",
  LOAD_FAIL: "AUTH_LOAD_FAIL",
  CLEAR: "AUTH_CLEAR"
};

export const AUTH_MESSAGE = {
  SUCCESS: "Selamat datang kembali..",
  SUCCESS_LOGOUT: "Semoga hari mu menyenangkan..",
  FAIL: "",
};
