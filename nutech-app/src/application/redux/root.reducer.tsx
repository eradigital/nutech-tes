import {combineReducers} from 'redux';

// REDUCER
import form_handling from '@nutech-application/redux/form_handling/reducers/form_handling.reducer';
import auth from '@nutech-application/redux/auth/reducers';
import product from '@nutech-application/redux/product/reducers';

const rootReducer = combineReducers({
  auth,
  form_handling,
  product,
});

export default rootReducer;
export type RootState = ReturnType<typeof rootReducer>;
