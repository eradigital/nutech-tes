import { PRODUCT } from "@nutech-application/redux/product/constants/product.constant";

export const productInit = () => ({ type: PRODUCT.LOAD_INIT });
