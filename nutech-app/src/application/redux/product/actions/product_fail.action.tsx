import { PRODUCT } from "@nutech-application/redux/product/constants/product.constant";

export const productFail = (error: any) => ({
  type: PRODUCT.LOAD_FAIL,
  data: error.message
});
