import { PRODUCT } from "@nutech-application/redux/product/constants/product.constant";

export const productLoadSuccess = (payload: any) => ({
  type: PRODUCT.LOAD_SUCCESS,
  data: payload
});
