import { success_message } from "@nutech-core/utils/alert_notification.util";
import { createProductRepository } from "@nutech-domain/repository/product/create_product.repository";
import { ProductRequestModel } from "@nutech-domain/models/product/product_request.model";
import { PRODUCT_MESSAGE } from "@nutech-application/redux/product/constants/product.constant";
import { productInit } from "@nutech-application/redux/product/actions/product_init.action";
import { productFail } from "@nutech-application/redux/product/actions/product_fail.action";
import { getListProductMiddleware } from "./get_list_product.middleware";

export const createProductMiddleware = (data: any, history: any, keyword: string = '') => {
  return async (dispatch: any) => {
    dispatch(productInit());
    try {
      const response = await createProductRepository(ProductRequestModel.fromJson(data));
      if (response.success) {
        success_message(PRODUCT_MESSAGE.SUCCESS_CREATE);
        dispatch(getListProductMiddleware(history, 1, keyword));
      }
    } catch (error) {
      console.log(JSON.stringify(error.message));
      dispatch(productFail(error));
    }
  };
};