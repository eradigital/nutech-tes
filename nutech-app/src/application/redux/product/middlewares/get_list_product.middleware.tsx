import { productLoadSuccess } from "@nutech-application/redux/product/actions/product_load_success.action";
import { STORAGE } from "@nutech-core/constants/storage/local.storage";
import { ROUTES } from "@nutech-core/constants/routes/list_route.constant";
import { productInit } from "@nutech-application/redux/product/actions/product_init.action";
import { productFail } from "@nutech-application/redux/product/actions/product_fail.action";
import { LocalStorage } from "@nutech-core/services/local_storage.service";
import { getProductRepository } from "@nutech-domain/repository/product/get_product.repository";

export const getListProductMiddleware = (history: any, page: number, keyword: string) => {
  return async (dispatch: any) => {
    dispatch(productInit());
    try {
      const response = await getProductRepository(page.toString(), keyword);

      if (response.success) {
        history.push(ROUTES.LIST_PRODUCT);
        await LocalStorage.setDataObjectStorage(STORAGE.PRODUCT, response.data);
        dispatch(productLoadSuccess(response.data));
      }
    } catch (error) {
      console.log(JSON.stringify(error.message));
      dispatch(productFail(error));
    }
  };
};