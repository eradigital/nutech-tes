export const PRODUCT = {
  LOAD_INIT: "PRODUCT_LOAD_INIT",
  LOAD_SUCCESS: "PRODUCT_LOAD_SUCCESS",
  LOAD_FAIL: "PRODUCT_LOAD_FAIL",
  CLEAR: "PRODUCT_CLEAR"
};

export const PRODUCT_MESSAGE = {
  SUCCESS_CREATE: "Data produk berhasil di tambahkan",
  SUCCESS_UPDATE: "Data produk berhasil di update",
  SUCCESS_DELETE: "Data produk berhasil di delete",
  SUCCESS_GET: "Data produk berhasil di dapatkan",
  FAIL: "",
};
