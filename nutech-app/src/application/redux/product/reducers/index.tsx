import { PRODUCT } from "@nutech-application/redux/product/constants/product.constant";
import { STATE_STATUS } from "@nutech-core/constants/status_state/status_state.constant";

const initialState = {
  posts: null,
  status: STATE_STATUS.INIT,
  error: null,
};

const product = (state = initialState, action: any) => {
  if (action.type === PRODUCT.LOAD_INIT) {
    const newState = {
      posts: null,
      status: STATE_STATUS.LOADING,
      error: null,
    };
    return newState;
  } else if (action.type === PRODUCT.LOAD_SUCCESS) {
    const newState = {
      posts: action.data,
      status: STATE_STATUS.SUCCESS,
      error: null,
    };
    return newState;
  } else if (action.type === PRODUCT.LOAD_FAIL) {
    const newState = {
      posts: null,
      status: STATE_STATUS.FAIL,
      error: action.data,
    };
    return newState;
  } else if (action.type === PRODUCT.CLEAR) {
    return initialState;
  }
  
  return state;
};

export default product;
