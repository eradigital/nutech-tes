<?php

namespace App\Http\Requests\Image;

use Anik\Form\FormRequest;
use App\Http\Controllers\ApiController;
use Illuminate\Http\JsonResponse;

class ImageStoreRequest extends FormRequest
{
    protected $apiController;
    public function __construct(ApiController $apiController)
    {
        $this->apiController = $apiController;
    }


    protected function authorize(): bool
    {
        return $this->request->has('unauthorized') ? false : parent::authorize();
    }

    protected function errorMessage(): string
    {
        return $this->get('error_message') ?? parent::errorMessage();
    }

    protected function statusCode(): int
    {
        return $this->get('status_code') ?? parent::statusCode();
    }

    protected function errorResponse(): ?JsonResponse
    {
        if (!$this->has('response')) {
            return $this->apiController->errorResponse('Mohon lengkapi data input', parent::errorResponse());
        }

        return $this->apiController->errorResponse('Data yang di masukan tidak dapat di proses', [
            'note' => $this->errorMessage(),
            'faults' => $this->validator->errors()->messages(),
        ]);
    }

    protected function rules(): array
    {
        return [
            'image' => 'required|mimes:png,jpg|max:100',
        ];
    }

    protected function messages(): array
    {
        if (!$this->has('message')) {
            return parent::messages();
        }

        return [
            'image.required' => 'image wajib di isi.',
            'image.mimes' => 'image harus dalam format png atau jpg.',
            'image.max' => 'image harus berukuran di bawah 100KB.',
        ];
    }

    protected function attributes(): array
    {
        if (!$this->has('attribute')) {
            return parent::attributes();
        }

        return [
            'username' => '',
        ];
    }
}
