<?php

namespace App\Http\Requests\User;

use App\Models\User;
use Anik\Form\FormRequest;
use App\Http\Controllers\ApiController;
use Illuminate\Http\JsonResponse;

class UserStoreRequest extends FormRequest
{
    protected $apiController;
    public function __construct(ApiController $apiController)
    {
        $this->apiController = $apiController;
    }


    protected function authorize(): bool
    {
        return $this->request->has('unauthorized') ? false : parent::authorize();
    }

    protected function errorMessage(): string
    {
        return $this->get('error_message') ?? parent::errorMessage();
    }

    protected function statusCode(): int
    {
        return $this->get('status_code') ?? parent::statusCode();
    }

    protected function errorResponse(): ?JsonResponse
    {
        if (!$this->has('response')) {
            return $this->apiController->errorResponse('Mohon lengkapi data input', parent::errorResponse());
        }

        return $this->apiController->errorResponse('Data yang di masukan tidak dapat di proses', [
            'note' => $this->errorMessage(),
            'faults' => $this->validator->errors()->messages(),
        ]);
    }

    protected function rules(): array
    {
        $user_auth = auth()->user();
        $user = json_decode(json_encode($user_auth), true);
        $user_id = $user["id"];
        $user_data = User::find($user_id);
        $msg_validator = [];

        if ($user_data->email == request()->email) {
            $rule_msg_validator = [
                'username' => 'required|unique:users',
            ];
            array_push($msg_validator, $rule_msg_validator);
        }

        if ($user_data->email != request()->email) {
            $rule_msg_validator = [
                'email' => 'required|email|unique:users',
                'username' => 'required|unique:users',
            ];
            array_push($msg_validator, $rule_msg_validator);
        }

        return $msg_validator[0];
    }

    protected function messages(): array
    {
        if (!$this->has('message')) {
            return parent::messages();
        }

        return [
            'email.unique' => 'Mohon masukan email lain, email saat ini telah di gunakan.',
            'username.unique' => 'Mohon masukan username lain, username saat ini telah di gunakan.',
        ];
    }

    protected function attributes(): array
    {
        if (!$this->has('attribute')) {
            return parent::attributes();
        }

        return [
            'username' => '',
        ];
    }
}
