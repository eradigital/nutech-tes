<?php

namespace App\Http\Requests\Product;

use Anik\Form\FormRequest;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\ApiController;

class ProductStoreRequest extends FormRequest
{
    protected $apiController;
    public function __construct(ApiController $apiController)
    {
        $this->apiController = $apiController;
    }


    protected function authorize(): bool
    {
        return $this->request->has('unauthorized') ? false : parent::authorize();
    }

    protected function errorMessage(): string
    {
        return $this->get('error_message') ?? parent::errorMessage();
    }

    protected function statusCode(): int
    {
        return $this->get('status_code') ?? parent::statusCode();
    }

    protected function errorResponse(): ?JsonResponse
    {
        if (!$this->has('response')) {
            return $this->apiController->errorResponse('Mohon lengkapi data input', parent::errorResponse());
        }

        return $this->apiController->errorResponse('Data yang di masukan tidak dapat di proses', [
            'note' => $this->errorMessage(),
            'faults' => $this->validator->errors()->messages(),
        ]);
    }

    protected function rules(): array
    {
        $id = request()->id;
        $product = DB::table('products')->where('id', '=', $id)->get();
        $isSameName = $id ? $product[0]->name_product == request()->name_product : false;
        $msg_validator = [];

        if ($isSameName) {
            $rule_msg_validator = [
                'file_image_id' => 'required',
                'purchase_price' => 'required|numeric',
                'selling_price' => 'required|numeric',
                'stock' => 'required|numeric',
            ];
            array_push($msg_validator, $rule_msg_validator);
        }

        if (!$isSameName) {
            $rule_msg_validator = [
                'file_image_id' => 'required',
                'name_product' => 'required|unique:products',
                'purchase_price' => 'required|numeric',
                'selling_price' => 'required|numeric',
                'stock' => 'required|numeric',
            ];
            array_push($msg_validator, $rule_msg_validator);
        }

        return $msg_validator[0];
    }

    protected function messages(): array
    {
        if (!$this->has('message')) {
            return parent::messages();
        }

        return [
            'file_image_id.required' => 'email wajib di isi.',
            'name_product.required' => 'username wajib di isi.',
            'purchase_price.required' => 'username wajib di isi.',
            'selling_price.required' => 'username wajib di isi.',
            'stock.required' => 'username wajib di isi.',
            'purchase_price.numeric' => 'purchase_price harus berupa nomor.',
            'selling_price.numeric' => 'selling_price harus berupa nomor.',
            'stock.numeric' => 'stock harus berupa nomor.',
            'name_product.unique' => 'Mohon masukan nama produk lain, nama produk saat ini telah di gunakan.',
        ];
    }

    protected function attributes(): array
    {
        if (!$this->has('attribute')) {
            return parent::attributes();
        }

        return [
            'username' => '',
        ];
    }
}
