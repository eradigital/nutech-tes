<?php

namespace App\Http\Requests\Auth;

use Anik\Form\FormRequest;
use App\Http\Controllers\ApiController;
use Illuminate\Http\JsonResponse;

class RegisterStoreRequest extends FormRequest
{
    protected $apiController;
    public function __construct(ApiController $apiController)
    {
        $this->apiController = $apiController;
    }


    protected function authorize(): bool
    {
        return $this->request->has('unauthorized') ? false : parent::authorize();
    }

    protected function errorMessage(): string
    {
        return $this->get('error_message') ?? parent::errorMessage();
    }

    protected function statusCode(): int
    {
        return $this->get('status_code') ?? parent::statusCode();
    }

    protected function errorResponse(): ?JsonResponse
    {
        if (!$this->has('response')) {
            return $this->apiController->errorResponse('Mohon lengkapi data input', parent::errorResponse());
        }

        return $this->apiController->errorResponse('Data yang di masukan tidak dapat di proses', [
            'note' => $this->errorMessage(),
            'faults' => $this->validator->errors()->messages(),
        ]);
    }

    protected function rules(): array
    {
        return [
            'email' => 'required|email|unique:users',
            'password' => 'required',
            'role_user' => 'required',
            'confirm_password' => 'required|same:password',
        ];
    }

    protected function messages(): array
    {
        if (!$this->has('message')) {
            return parent::messages();
        }

        return [
            'email.unique' => 'Mohon masukan email lain, email saat ini telah di gunakan.',
            'password.required' => 'password wajib di isi.',
            'role_user.required' => 'role_user wajib di isi.',
            'confirm_password.required' => 'confirm_password wajib di isi.',
        ];
    }

    protected function attributes(): array
    {
        if (!$this->has('attribute')) {
            return parent::attributes();
        }

        return [
            'username' => '',
        ];
    }
}
