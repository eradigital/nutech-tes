<?php

namespace App\Http\Controllers\Product;

use App\Http\Controllers\ApiController;
use App\Http\Requests\Product\ProductStoreRequest;
use Illuminate\Http\Request;
use App\Repositories\ProductRepositoryInterface;

class ProductController extends ApiController
{
    public $productRepository;
    public function __construct(ProductRepositoryInterface $productRepository)
    {
        $this->productRepository = $productRepository;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try {
            $keyword = request()->keyword;
            $products = $this->productRepository->getAll($keyword);
            return $this->successResponse('Daftar produk berhasil di dapatkan', $products);
        } catch (Exception $e) {
            return $this->errorResponse($e->getMessage(), $e);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ProductStoreRequest $request)
    {
        try {
            $result = $this->productRepository->create($request);
            return $this->successResponse('Data produk berhasil di tambahkan', $result);
        } catch (Exception $e) {
            return $this->errorResponse($e->getMessage(), $e);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try {
            $result = $this->productRepository->getById($id);
            return $this->successResponse('Detail produk berhasil di dapatkan', $result);
        } catch (Exception $e) {
            return $this->errorResponse($e->getMessage(), $e);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(ProductStoreRequest $request, $id)
    {
        try {
            $result = $this->productRepository->update($id, $request);
            return $this->successResponse('Data produk berhasil di update', $result);
        } catch (Exception $e) {
            return $this->errorResponse($e->getMessage(), $e);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $result = $this->productRepository->delete($id);
            return $this->successResponse('Data produk berhasil di hapus', $result);
        } catch (Exception $e) {
            return $this->errorResponse($e->getMessage(), $e);
        }
    }
}
