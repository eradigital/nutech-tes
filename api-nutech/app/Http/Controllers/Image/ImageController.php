<?php

namespace App\Http\Controllers\Image;

use Illuminate\Http\Request;
use App\Http\Controllers\ApiController;
use Illuminate\Support\Facades\Storage;
use App\Http\Requests\Image\ImageStoreRequest;
use App\Repositories\ImageRepositoryInterface;

class ImageController extends ApiController
{
    public $imageRepository;
    public function __construct(ImageRepositoryInterface $imageRepository)
    {
        $this->imageRepository = $imageRepository;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ImageStoreRequest $request)
    {
        if ($request->hasFile('image')) {
            $response = $this->imageRepository->create($request);

            return $this->successResponse('File telah berhasil di upload.', $response);
        } else {
            return $this->errorResponse('Mohon upload file image', null);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $path = $this->imageRepository->getById($id);
        if (Storage::exists($path)) {
            $response['file_deleted'] = Storage::delete($path);
            $response['row_table_deleted'] = $this->imageRepository->delete($id);
            return $this->successResponse('gambar berhasil di hapus', $response);
        } else {
            return $this->errorResponse('File tidak ada atau sudah terhapus', null);
        }
    }
}
