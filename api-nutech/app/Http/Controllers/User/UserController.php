<?php

namespace App\Http\Controllers\User;

use Illuminate\Http\Request;
use App\Http\Controllers\ApiController;
use App\Http\Requests\User\UserStoreRequest;
use App\Repositories\UserRepositoryInterface;

class UserController extends ApiController
{
    public $userRepository;
    public function __construct(UserRepositoryInterface $userRepository)
    {
        $this->userRepository = $userRepository;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try {
            $users = $this->userRepository->getAll();
            return $this->successResponse('Daftar user berhasil di dapatkan', $users);
        } catch (Exception $e) {
            return $this->errorResponse($e->getMessage(), $e);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show()
    {
        try {
            $user_auth = auth()->user();
            $user = json_decode(json_encode($user_auth), true);
            $user_data = $this->userRepository->getById($user['id']);

            return $this->successResponse('Detail user berhasil di dapatkan', $user_data);
        } catch (Exception $e) {
            return $this->errorResponse($e->getMessage(), $e);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UserStoreRequest $request)
    {
        try {
            $user_auth = auth()->user();
            $user = json_decode(json_encode($user_auth), true);
            $user_id = $user["id"];

            $users = $this->userRepository->update($user_id, $request);
            return $this->successResponse('Data akun telah berhasil perbaharui', $users);
        } catch (Exception $e) {
            return $this->errorResponse($e->getMessage(), $e);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $isAdmin = true;
            if ($isAdmin) {
                $user = $this->userRepository->delete($id);
                return $this->successResponse('Data user telah berhasil di hapus', $user);
            } else {
                return $this->notAccessResponse();
            }
        } catch (Exception $e) {
            return $this->errorResponse($e->getMessage(), $e);
        }
    }
}
