<?php

namespace App\Http\Controllers;

use GuzzleHttp\Client;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use App\Http\Controllers\ApiController;
use App\Http\Requests\Auth\LoginStoreRequest;
use App\Repositories\AuthRepositoryInterface;
use App\Http\Requests\Auth\RegisterStoreRequest;

class AuthController extends ApiController
{
    protected $authRepository;
    public function __construct(AuthRepositoryInterface $authRepository)
    {
        $this->authRepository = $authRepository;
    }

    public function register(RegisterStoreRequest $request)
    {
        try {
            $result = $this->authRepository->register($request);

            if ($result) {
                return $this->generateToken($request);
            }
        } catch (Exception $e) {
            return $this->errorResponse($e->getMessage(), $e);
        }
    }

    public function login(LoginStoreRequest $request)
    {
        try {
            return $this->generateToken($request);
        } catch (Exception $e) {
            return $this->errorResponse($e->getMessage(), $e);
        }
    }

    public function generateToken(Request $request)
    {
        try {
            $data_user = $this->authRepository->login($request);

            if ($data_user) {
                $password = $request->password;

                if (Hash::check($password, $data_user['password'])) {
                    $token = $this->authRepository->createToken($request);
                    return $this->successResponse('Anda telah berhasil masuk', ['token' => $token, 'user_data' => $data_user]);
                } else {
                    return $this->errorResponse('Password tidak sesuai', null);
                }
            } else {
                return $this->errorResponse('Email tidak di temukan', null);
            }
        } catch (Exception $e) {
            return $this->errorResponse($e->getMessage(), $e);
        }
    }

    public function logout()
    {
        try {
            if (Auth::check()) {
                Auth::user()->token()->revoke();
                return $this->successResponse('Sampai jumpa kembali...', 'logout_success');
            } else {
                return response()->json(['error' => 'api.something_went_wrong'], 500);
            }
        } catch (\Exception $e) {
            return $this->errorResponse($e->getMessage(), null);
        }
    }
}
