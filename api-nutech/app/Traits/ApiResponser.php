<?php

namespace App\Traits;

trait ApiResponser {
    public function successResponse($message, $data){
        return response()->json([
            'success' => true,
            'message' => $message,
            'data' => $data,
        ], 200);
    }

    public function unauthorizedResponse(){
        return response()->json([
            'success' => false,
            'message' => 'Unauthorized',
            'data' => null,
        ], 401);
    }

    public function errorResponse($message, $data){
        return response()->json([
            'success' => false,
            'message' => $message,
            'data' => $data,
        ], 400);
    }

    public function notLoginResponse(){
        return response()->json([
            'success' => false,
            'message' => 'Silahkan login terlebih dahulu',
            'data' => null,
        ], 401);
    }

    public function timeOutResponse(){
        return response()->json([
            'success' => false,
            'message' => 'Permintaan anda terkena timeout, silahkan coba lagi beberapa saat lagi',
            'data' => null,
        ], 408);
    }

    public function notAccessResponse(){
        return response()->json([
            'success' => false,
            'message' => 'Anda tidak mempunyai akses ke file atau direktory yang di minta',
            'data' => null,
        ], 403);
    }

    public function unProsesEntityResponse($message, $data){
        return response()->json([
            'success' => false,
            'message' => $message,
            'data' => $data,
        ], 422);
    }

    public function badResponse($data){
        return response()->json([
            'success' => false,
            'message' => 'Terjadi kesalahan pada syntax code program',
            'data' => $data,
        ], 500);
    }

    public function serverDownResponse(){
        return response()->json([
            'success' => false,
            'message' => 'Mohon maaf server sedang sibuk atau sedang dalam maintanance',
            'data' => null,
        ], 503);
    }
}
