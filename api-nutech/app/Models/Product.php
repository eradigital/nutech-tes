<?php

namespace App\Models;
use App\Models\FileImage;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Product extends Model
{
    use HasFactory;
    protected $guarded = ['id'];

    public function file_image(){
        return $this->belongsTo(FileImage::class);
    }
}
