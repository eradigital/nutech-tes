<?php

namespace App\Models;

use Illuminate\Auth\Authenticatable;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Database\Eloquent\Model;
use Laravel\Lumen\Auth\Authorizable;
use Laravel\Passport\HasApiTokens;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class User extends Model implements AuthenticatableContract, AuthorizableContract
{
    use HasFactory, HasApiTokens, Authenticatable, Authorizable;

    const VERIFIED_USER = '1';
    const UNVERIFIED_USER = '0';

    const ADMIN_GRAND_ACCESS = '3';
    const ADMIN_CREATOR = '2';
    const STUDENT = '1';

    protected $table = 'users';

    protected $primaryKey = 'id';
    protected $guarded = array('id');
    public $timestamps = true;

    protected $hidden = [
        'password',
        'remember_token',
    ];

    public function setUsernameAttribute($username){
        $this->attributes['username'] = strtolower($username);
    }

    public function getUsernameAttribute($username){
        return ucwords($username);
    }

    public function setEmailAttribute($email){
        $this->attributes['email'] = strtolower($email);
    }

    public function isAdminGrandAccess(){
        return $this->role == User::ADMIN_GRAND_ACCESS;
    }

    public function isVerifiedUser(){
        return $this->role == User::VERIFIED_USER;
    }
}
