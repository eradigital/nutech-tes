<?php

use Carbon\Carbon;

function general_date_format($date)
{
    return Carbon::parse($date)->format('d M Y');
}
