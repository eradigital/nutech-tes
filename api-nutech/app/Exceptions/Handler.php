<?php

namespace App\Exceptions;

use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Database\QueryException;
use Illuminate\Validation\ValidationException;
use Laravel\Lumen\Exceptions\Handler as ExceptionHandler;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Exception;
use Illuminate\Http\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpKernel\Exception\MethodNotAllowedHttpException;
use Illuminate\Http\Exception\HttpResponseException;
use Symfony\Component\Debug\Exception\FatalThrowableError;
use Throwable;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that should not be reported.
     *
     * @var array
     */
    protected $dontReport = [
        AuthorizationException::class,
        HttpException::class,
        ModelNotFoundException::class,
        ValidationException::class,
    ];

    /**
     * Report or log an exception.
     *
     * This is a great spot to send exceptions to Sentry, Bugsnag, etc.
     *
     * @param  \Throwable  $exception
     * @return void
     *
     * @throws \Exception
     */
    public function report(Throwable $exception)
    {
        parent::report($exception);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Throwable  $exception
     * @return \Illuminate\Http\Response|\Illuminate\Http\JsonResponse
     *
     * @throws \Throwable
     */
    public function render($request, Throwable $e)
    {
        if (env('APP_DEBUG')) {
            return parent::render($request, $e);
        }
        $response['status']   = null;

        if ($e instanceof HttpResponseException) {
            $response['status']   = Response::HTTP_INTERNAL_SERVER_ERROR;
            $response['message'] = 'Terjadi kesalahan pada penulisan syntax code';
        } elseif ($e instanceof MethodNotAllowedHttpException) {
            $response['status'] = Response::HTTP_METHOD_NOT_ALLOWED;
            $response['message'] = 'Methode yang anda gunakan tidak di ijinkan';
        } elseif ($e instanceof NotFoundHttpException) {
            $response['status'] = Response::HTTP_NOT_FOUND;
            $response['message'] = 'Sumber atau halaman tidak di temukan silahkan check kembali link yang di gunakan';
        } elseif ($e instanceof AuthorizationException) {
            $response['status'] = Response::HTTP_FORBIDDEN;
            $response['message'] = 'Anda tidak di ijinkan untuk mengakses halaman ini';
            $e      = new AuthorizationException('HTTP_FORBIDDEN', $response['status']);
        } elseif ($e instanceof ValidationException && $e->getResponse()) {
            $response['status']   = Response::HTTP_BAD_REQUEST;
            $response['message'] = 'Cek kembali data request yang anda kirimkan';
            $e        = new ValidationException('HTTP_BAD_REQUEST', $response['status'], $e);
        } elseif ($e instanceof ValidationException) {
            $response['status']   = 422;
            $response['message'] = 'Error validasi input, mohon cek kembali input yang anda masukan';
            $response['errors'] = $e->validator->errors();
        } elseif ($e instanceof ModelNotFoundException) {
            $response['status'] = 404;
            $response['message'] = 'Model database tidak di temukan';
        } elseif ($e instanceof UnableToExecuteRequestException) {
            $response['status'] = $e->getCode();
            $response['message'] = 'Permintaan anda tidak dapat di proses, silahkan coba kembali';
        } elseif ($e instanceof FatalThrowableError) {
            $response['status'] = Response::HTTP_INTERNAL_SERVER_ERROR;
            $response['message'] = 'Terjadi kesalaha pada pada server, silahkan coba kembali beberapa saat lagi';
        } elseif ($e) {
            $response['status'] = Response::HTTP_INTERNAL_SERVER_ERROR;
            $response['message'] = 'Terjadi kesalaha pada pada server, IT kami akan segera memperbaikinya';
            $response['unhandled'] = 'exception-lumen-ksoft';
        } elseif ($e instanceof QueryException) {
            $errorCode = $e->errorInfo[1];

            if ($errorCode == 1451) {
                $response['status'] = 409;
                $response['message'] = 'Data tidak dapat di hapus di karenakan berelasi pada sumber lain';
                $response['unhandled'] = 'Tidak dapat menghapus sumber daya permanen. itu berelasi ke resource lainnya';
            };
        }

        if ($response['status']) {
            $response['error_code'] = $e->getCode() ?? '';
            $response['exception'] = $e->getTraceAsString() ?? '';
            if (app()->environment() == 'local') {
                $response['file'] = $e->getFile() ?? '';
                $response['line'] = $e->getLine() ?? '';
            }

            return response()->json([
                'success' => false,
                'message' => $response['message'],
                'data' => $response,
            ], $response['status']);
        } else {
            return response()->json([
                'success' => false,
                'message' => 'Unexpected Exception. Try later',
                'data' => $response,
            ], 500);
        }
    }
}
