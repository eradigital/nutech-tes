<?php

namespace App\Repositories;

interface ProductRepositoryInterface{
    public function getAll($keyword);
    public function getById($id);
    public function create($response);
    public function update($id, $response);
    public function delete($id);
}
