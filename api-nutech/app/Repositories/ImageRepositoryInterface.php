<?php

namespace App\Repositories;

interface ImageRepositoryInterface{
    public function getAll();
    public function getById($id);
    public function create($request);
    public function update($id, $request);
    public function delete($id);
}
