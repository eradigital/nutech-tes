<?php

namespace App\Repositories;

interface AuthRepositoryInterface{
    public function register($request);
    public function login($request);
    public function createToken($request);
}
