<?php

namespace App\Repositories\Eloquent;

use App\Models\FileImage;
use App\Http\Controllers\ApiController;
use App\Repositories\ImageRepositoryInterface;

class ImageRepository implements ImageRepositoryInterface
{
    protected $apiController;
    public function __construct(ApiController $apiController)
    {
        $this->apiController = $apiController;
    }

    public function getAll()
    {
    }

    public function getById($id)
    {
        return FileImage::find($id)->file_local;
    }

    public function create($request)
    {
        $fileName = time() . '_' . $request->file('image')->getClientOriginalName();
        $fileExtension = $request->file('image')->getClientOriginalExtension();
        $fileSize = $request->file('image')->getSize();
        $filePath = $request->file('image')->storeAs('uploads', $fileName);

        $response = FileImage::create([
            "name" => $fileName,
            "file_local" => $filePath,
            "file_url" => env('APP_BASE_URL'). '/storage/' . $fileName,
            "file_extension" => $fileExtension,
            "file_size" => $fileSize,
        ]);

        return $response;
    }

    public function update($id, $request)
    {
    }

    public function delete($id)
    {
        return FileImage::destroy($id);
    }
}
