<?php

namespace App\Repositories\Eloquent;

use App\Models\Product;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\ApiController;
use App\Repositories\ProductRepositoryInterface;

class ProductRepository implements ProductRepositoryInterface
{
    protected $apiController;
    public function __construct(ApiController $apiController)
    {
        $this->apiController = $apiController;
    }

    public function getAll($keyword)
    {
        return Product::with('file_image')
            ->where('name_product', 'LIKE', "%{$keyword}%")
            ->orWhere('stock', 'LIKE', "%{$keyword}%")
            ->paginate(5);
    }

    public function getById($id)
    {
        return Product::find($id);
    }

    public function create($request)
    {
        $input = $request->all();
        return Product::create($input);
    }

    public function update($id, $request)
    {

        $product_data = Product::find($id);

        $product_data->file_image_id = $request->file_image_id;
        $product_data->name_product = $request->name_product;
        $product_data->purchase_price = $request->purchase_price;
        $product_data->selling_price = $request->selling_price;
        $product_data->stock = $request->stock;

        if ($product_data->isClean()) {
            return $this->apiController->errorResponse('Tidak ada perubahan data yang anda masukan', null);
        }

        $response = tap(DB::table('products')->where('id', $id))
            ->update($request->all())
            ->first();

        return $response;
    }

    public function delete($id)
    {
        $product = Product::findOrFail($id);
        $product->delete();
        return $product;
    }
}
