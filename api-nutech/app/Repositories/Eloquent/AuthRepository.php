<?php

namespace App\Repositories\Eloquent;

use App\Models\User;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\Hash;
use App\Http\Controllers\ApiController;
use App\Repositories\AuthRepositoryInterface;

class AuthRepository implements AuthRepositoryInterface
{
    protected $apiController;
    public function __construct(ApiController $apiController)
    {
        $this->apiController = $apiController;
    }

    public function register($request)
    {
        $input = $request->all();

        unset($input['confirm_password']);
        $input['password'] = Hash::make($input['password']);
        $input['verified'] = User::UNVERIFIED_USER;
        $input['verification_token'] = Hash::make($input['email']);

        return User::create($input);
    }

    public function login($request)
    {
        return User::where('email', '=', $request->email)->first();
    }

    public function createToken($request)
    {
        $client = new Client();
        $response = $client->post(config('service.passport.login_endpoint'), [
            "form_params" => [
                "client_secret" => config('service.passport.client_secret'),
                "grant_type" => "password",
                "client_id" => config('service.passport.client_id'),
                "username" => $request->email,
                "password" => $request->password
            ]
        ]);

        $token = json_decode((string)$response->getBody());
        return $token;
    }
}
