<?php

namespace App\Repositories\Eloquent;

use App\Models\User;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\ApiController;
use App\Repositories\UserRepositoryInterface;

class UserRepository implements UserRepositoryInterface
{
    protected $apiController;
    public function __construct(ApiController $apiController)
    {
        $this->apiController = $apiController;
    }

    public function getAll()
    {
        return User::all();
    }
    public function getById($id)
    {
        return User::find($id);
    }
    public function create($attributes)
    {
    }
    public function update($id, $request)
    {

        $user_data = User::find($id);

        $user_data->email = $request->email;
        $user_data->username = $request->username;

        if ($user_data->isClean()) {
            return $this->apiController->errorResponse('Tidak ada perubahan data yang anda masukan', null);
        }

        $response = tap(DB::table('users')->where('id', $id))
            ->update($request->all())
            ->first();

        return $response;
    }
    public function delete($id)
    {
        $user = User::findOrFail($id);
        $user->delete();
        return $user;
    }
}
