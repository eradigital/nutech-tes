<?php

namespace Database\Factories;

use App\Models\Product;
use App\Models\FileImage;
use Illuminate\Database\Eloquent\Factories\Factory;

class ProductFactory extends Factory
{
    protected $model = Product::class;

    public function definition(): array
    {
        return [
            'file_image_id' => FileImage::all()->random()->id,
            'name_product' => $this->faker->name,
            'purchase_price' => $this->faker->randomElement([2000, 30000, 40000, 55000]),
            'selling_price' => $this->faker->randomElement([2000, 30000, 40000, 55000]),
            'stock' => $this->faker->randomElement([200, 300, 400]),
        ];
    }
}
