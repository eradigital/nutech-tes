<?php
namespace Database\Factories;

use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

class UserFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = User::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        static $password;

        return [
            'email' => $this->faker->unique()->safeEmail,
            'username' => $this->faker->name,
            'password' => $password ?: $password = app('hash')->make('yourpassword'),
            'remember_token' => Str::random(12),
            'verified' => $verified = $this->faker->randomElement([User::VERIFIED_USER, User::UNVERIFIED_USER]),
            'verification_token' => Str::random(12),
            'role_user' => $verified = $this->faker->randomElement([User::ADMIN_GRAND_ACCESS, User::ADMIN_CREATOR]),
        ];
    }
}
