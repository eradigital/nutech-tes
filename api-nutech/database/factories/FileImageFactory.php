<?php

namespace Database\Factories;

use App\Models\FileImage;
use Illuminate\Database\Eloquent\Factories\Factory;

class FileImageFactory extends Factory
{
    protected $model = FileImage::class;

    public function definition(): array
    {
        return [
            'name' => $this->faker->randomElement(['example_001.png', 'example_002.png', 'example_003.png']),
            'file_local' => $this->faker->randomElement(['example_001.png', 'example_002.png', 'example_003.png']),
            'file_url' => $this->faker->randomElement(['example_001.png', 'example_002.png', 'example_003.png']),
            'file_extension' => $this->faker->randomElement(['png', 'jpg', 'jpeg']),
            'file_size' => $this->faker->randomElement([200, 300, 400]),
        ];
    }
}
