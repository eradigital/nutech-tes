<?php

use App\Models\User;
use App\Models\Product;
use App\Models\FileImage;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS = 0');

        User::truncate();
        FileImage::truncate();
        Product::truncate();

        User::factory()->count(20)->create();
        FileImage::factory()->count(20)->create();
        Product::factory()->count(20)->create();
    }
}
