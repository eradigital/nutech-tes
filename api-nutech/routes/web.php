<?php

/** @var \Laravel\Lumen\Routing\Router $router */

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    return $router->app->version();
});

$router->group(['prefix' => 'user'], function () use ($router) {
    $router->post('/register', 'AuthController@register');
    $router->post('/login', 'AuthController@login');

    $router->group(['middleware' => 'auth'], function () use ($router) {
        $router->get('/', 'User\UserController@index');
        $router->get('/detail', 'User\UserController@show');
        $router->put('/update', 'User\UserController@update');
        $router->delete('/delete/{id}', 'User\UserController@destroy');
        $router->post('/logout', 'AuthController@logout');
    });
});

// $router->group(['middleware' => 'auth'], function () use ($router) {
$router->group([], function () use ($router) {
    $router->group(['prefix' => 'image'], function () use ($router) {
        $router->post('/upload', 'Image\ImageController@store');
        $router->delete('/delete/{id}', 'Image\ImageController@destroy');
    });

    $router->group(['prefix' => 'product'], function () use ($router) {
        $router->get('/', 'Product\ProductController@index');
        $router->get('/{id}', 'Product\ProductController@show');
        $router->post('/', 'Product\ProductController@store');
        $router->put('/{id}', 'Product\ProductController@update');
        $router->delete('/{id}', 'Product\ProductController@destroy');
    });
});
